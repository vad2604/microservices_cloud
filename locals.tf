locals {

  runner_tokens = {

    for runner_key, runner_token in var.runner_tokens : runner_key => {

      token: runner_token
    }
  }

  pull_secret_passwords = {

    for ps_key, ps_password in var.pull_secret_passwords : ps_key => {

      password: ps_password
    }
  }

  runners = {

    for runner_key, runner_config in var.gitlab_runner_config.runners : runner_key => merge(runner_config, lookup(local.runner_tokens, runner_key))
  }

  pull_secrets = {

    for ps_key, ps_config in var.gitlab_runner_config.pull_secrets : ps_key => merge(ps_config, lookup(local.pull_secret_passwords, ps_key))
  }

  gitlab_runner_config = {

    name : var.gitlab_runner_config.name
    version : var.gitlab_runner_config.version
    namespace : var.gitlab_runner_config.namespace
    create_ns : var.gitlab_runner_config.create_ns
    helm_repo : var.gitlab_runner_config.helm_repo

    runners : local.runners
    pull_secrets : local.pull_secrets
  }
}
