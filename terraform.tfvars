# конфигурация облаков и их фолдеров
clouds_config = {

  msa-cloud = {

    description = "Облако для курса микросервисная архитектура"

    cloud_users = {

      "vad2604@yandex.ru" = {
          cloud_user_roles = [ "admin" ]
      }
    }

    labels = {
      "created-by" = "terraform"
      "environment" = "multi"
    }
  }

}



# конфигурация фолдеров
folders_config = {

  msa-folder = {

      description   = "Папка для нашего окружения"

      cloud_name    = "msa-cloud"

      folder_users  = {

          "vad2604@yandex.ru" = {
              folder_user_roles = [ "admin" ]
          }
      }

      labels = {
          "created-by" = "terraform"
          "environment" = "multi"
      }
  }

}

cloud_logging_config = {/*

  msa-k8s-main-cluster = {
    description       = "Logging группа для нашего окружения"

    cloud_name = "msa-cloud"
    folder_name = "msa-folder"

    labels = {
      "created-by"    = "terraform"
      "environment"   = "multi"
    }
  }*/
}

dns_zones_config = {

  "example-com" = {

    description = "Публичная DNS зона для окружения"
    dns_zone = "cs14018.tw1.ru"
    public = true
    cloud_name = "msa-cloud"
    folder_name = "msa-folder"

    labels = {
      "created-by" = "terraform"
      "environment" = "multi"
    }
  }
}

networks_config = {

  msa-network = {

    description = "Общая сеть для инфраструктуры"

    cloud_name  = "msa-cloud"
    folder_name = "msa-folder"

    gateway_description = "Шлюз для доступа в интернет"
    route_table_description = "Маршрут в интернет"

    subnets = {

      "k8s-subnet-1" = {

        v4_cidr_blocks    = ["10.1.0.0/16"]
        availability_zone = "ru-central1-a"
        use_nat           = true
      }

      "k8s-subnet-2" = {

        v4_cidr_blocks    = ["10.2.0.0/16"]
        availability_zone = "ru-central1-b"
        use_nat           = true
      }

      "k8s-subnet-3" = {

        v4_cidr_blocks    = ["10.3.0.0/16"]
        availability_zone = "ru-central1-c"
        use_nat           = true
      }

      "psql-subnet-1" = {

        v4_cidr_blocks    = ["10.10.1.0/24"]
        availability_zone = "ru-central1-a"
        use_nat           = false
      }
/*
      "psql-subnet-2" = {

        v4_cidr_blocks    = ["10.10.2.0/24"]
        availability_zone = "ru-central1-b"
        use_nat           = false
      }

      "psql-subnet-3" = {

        v4_cidr_blocks    = ["10.10.3.0/24"]
        availability_zone = "ru-central1-c"
        use_nat           = false
      }
*/
      "mongo-subnet-1" = {

        v4_cidr_blocks    = ["10.10.4.0/24"]
        availability_zone = "ru-central1-a"
        use_nat           = false
      }
/*
      "mongo-subnet-2" = {

        v4_cidr_blocks    = ["10.10.5.0/24"]
        availability_zone = "ru-central1-b"
        use_nat           = false
      }

      "mongo-subnet-3" = {

        v4_cidr_blocks    = ["10.10.6.0/24"]
        availability_zone = "ru-central1-c"
        use_nat           = false
      }
*/
      "kafka-subnet-1" = {

        v4_cidr_blocks    = ["10.10.7.0/24"]
        availability_zone = "ru-central1-a"
        use_nat           = false
      }

      "kafka-subnet-2" = {

        v4_cidr_blocks    = ["10.10.8.0/24"]
        availability_zone = "ru-central1-b"
        use_nat           = false
      }

      "kafka-subnet-3" = {

        v4_cidr_blocks    = ["10.10.9.0/24"]
        availability_zone = "ru-central1-c"
        use_nat           = false
      }

      # "new-subnet" = {
        #....
        # подобным образом можно указывать конфигурации для создания дополнительных подсетей
        #....
      #}
    }

    labels = {
      "created-by" = "terraform"
      "environment" = "multi"
    }
  }
}

k8s_config = {

    description         = "Кластер для курса Микросервисная архитектура"
    name                = "msa-k8s-cluster"

    cloud_name          = "msa-cloud"
    folder_name         = "msa-folder"
    network_name        = "msa-network"

    cluster_ipv4_range            = "10.4.0.0/16"
    service_ipv4_range            = "10.5.0.0/16"
    zonal_regional                = "zonal"
    public_ip                     = true
    k8s_master_version            = "1.23"
    maintenance_window_day        = "sunday"
    maintenance_window_start_time = "02:00"
    maintenance_window_duration   = "3h"
    prevent_destroy               = false

    ns_create_list = [
      "dev",
      "preprod",
      "prod"
    ]

    cluster_sa_roles = [
      "k8s.editor",
      "k8s.clusters.agent",
      "kms.editor",
      "load-balancer.admin",
      "vpc.publicAdmin",
      "compute.admin",
      "logging.writer",
      "monitoring.editor",
      "container-registry.images.puller"
    ]

    node_groups_sa_roles = [
      "k8s.admin",
      "k8s.cluster-api.cluster-admin",
      "k8s.clusters.agent",
      "container-registry.images.puller",
      "vpc.publicAdmin",
      "compute.admin",
      "logging.writer",
      "monitoring.editor",
    ]

    subnet_names = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3",
    ]

    access_22_cidrs  = [
      "0.0.0.0/0"
    ]

    access_API_cidrs = [
      "0.0.0.0/0"
    ]

    access_22_subnets = []

    access_API_subnets = []

    access_ingress_whitelist = {
      "key" = {
        description = "value"
        from_port = 64000
        protocol = "UDP"
        to_port = 64000
        v4_cidr_blocks = [ "10.10.10.3/32" ]
      }
    }

    labels = {
      "created-by" = "terraform"
      "environment" = "multi"
    }

    node_groups = {

      node-group-1 = {

        description = "Группа k8s-nodes #1 dev среды"

        # В каждом сабнете из списка будет создана node group
        #subnet_name = [
        #  "k8s-subnet-1",
        #  "k8s-subnet-2",
        #  "k8s-subnet-3"
        #]

        node_taints = []
        network_acceleration = false

        k8s_node_version              = "1.23"
        resources_memory              = 4
        resources_cpu_cores           = 2
        boot_disk_type                = "network-ssd"
        boot_disk_size                = 50
        current_node_count            = 1
        auto_scale_min_count          = 1 # можно не указывать для реионального кластера,
        auto_scale_max_count          = 3 # так как поддерживается только в зональных
        maintenance_window_day        = "sunday"
        maintenance_window_start_time = "02:00"
        maintenance_window_duration   = "3h"

        node_labels                   = {
          "node.kubernetes.io/worker" = "true"
        }

        labels = {
          "created-by" = "terraform"
          "environment" = "multi"
        }
      }

      # node-group-2 = {
      #   подобным образом можно указывать конфигурации для дополнительных груп нод
      # }
    }
}

buckets_config = {

  msa-storage = {

    cloud_name = "msa-cloud"
    folder_name = "msa-folder"

    sa_list = {}
    lifecycle_rules = []
  }
}

psql_config = {

  msa-psql = {

    description = "Кластер Postgres"

    cloud_name          = "msa-cloud"
    folder_name         = "msa-folder"
    network_name        = "msa-network"
    admin_name          = "rootuser"
    version             = 12
    deletion_protection = false
    public_access = false

    resource_preset_id = "s3-c2-m8"
    user_conn_limit = 20
    max_connections = 400
    disk_size = 10
    disk_type_id = "network-ssd"

    hosts = {
      h1 = {
        subnet_name = "psql-subnet-1"
      }
    }

    access_6432_cidrs = []
    access_6432_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3",
    ]

    backup_window_start = {
      hours = 2
      minutes = 0
    }

    maintenance_window = null

    labels = {}

    users = {

      user-1 = {

        accessible_databases = [
            "db1"
        ]
      }
    }

    databases = {

      "db1" = {

        extension = []
        owner = "user-1"
      }
    }
  }
}

mongo_config = {

  msa-mongo = {

    description = "Пример MongoDB"

    cloud_name   = "msa-cloud"
    folder_name  = "msa-folder"
    network_name = "msa-network"
    admin_name   = "rootuser"
    deletion_protection = false
    public_access       = false

    subnet_name = [
      "mongo-subnet-1",
    ]

    access_27018_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3",
    ]

    access_27018_cidrs = []

    resource_preset_id = "s3-c2-m8"
    disk_size = 10
    disk_type_id = "network-ssd"

    users = {
      user-1 = {
          username = "test"
          accessible_databases = [{
              database_name = "test"
              roles         = [
                  "mdbDbAdmin",
              ]
          }]
      }
    }

    databases = [ "test" ]

    backup_window_start = {
      hours = 3
      minutes = 0
    }

    labels = {
      "env" = "prod"
    }

    maintenance_window = null

  }
}


kafka_config = {

  msa-kafka = {
    description = "Пример Kafka"

    cloud_name   = "msa-cloud"
    folder_name  = "msa-folder"
    network_name = "msa-network"
    deletion_protection = false
    public_access = true

    brokers_count = 1
    unmanaged_topics = false
    schema_registry = false

    resource_preset_id = "s3-c2-m8"
    disk_type_id = "network-ssd"
    disk_size = 10

    subnet_name = [
      "kafka-subnet-1",/*
      "kafka-subnet-2",
      "kafka-subnet-3",*/
    ]

    zones = [
      "ru-central1-a",/*
      "ru-central1-b",
      "ru-central1-c", */
    ]

    access_9091_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3",
    ]

    access_9092_subnets = [
      "k8s-subnet-1",
      "k8s-subnet-2",
      "k8s-subnet-3",
    ]

    access_9091_cidrs = ["0.0.0.0/0"]
    access_9092_cidrs = ["0.0.0.0/0"]

    topics = {

      k8s-logging = {

        partitions = 4
        replication_factor = 1
      }
    }

    users = {

      msa-k8s-logging = {
        permissions = [{
          topic_name = "k8s-logging"
          role = "ACCESS_ROLE_CONSUMER"
        },{
          topic_name = "k8s-logging"
          role = "ACCESS_ROLE_PRODUCER"
        }]
      }
    }

    labels = {}

    maintenance_window = null
  }

}

cert_manager_config = {

  email = "vad2604@yandex.ru"
}

fluent_bit_config = {

  cloud_name   = "msa-cloud"
  folder_name  = "msa-folder"
  kafka_cluster_name = "msa-kafka"
  kafka_topic_name = "k8s-logging"
  kafka_user = "msa-k8s-logging"

  sa_name = "msa-k8s-fluent-bit-sa"

  sa_roles = []
}

ingress_config = {

  replicas = 1

  cloud_name   = "msa-cloud"
  folder_name  = "msa-folder"
  dns_zone_name = "example-com"
}

gitlab_runner_config = {

  helm_repo = "https://gitlab-charts.s3.amazonaws.com/"

  runners = {

   "gitlab-runner" = {

      url = "https://gitlab.com/"
      tags = [
        "gitlab-runner",
        "msa-gitlab-runner"
      ]
    }

  }

  pull_secrets = {

    "gitlab-registry" = {

      server = "registry.gitlab.com"
      username = "gitlab+deploy-token-1738371"

      namespaces = [
        "dev",
        "preprod",
        "prod",
        "gitlab-runner"
      ]
    }
  }
}
