terraform {

  # для хранения состояния указываем тип хранилища состояния http
  backend "http" {
  }

  required_providers {

    yandex  = {
      source        = "yandex-cloud/yandex"
      version       = "0.83.0"
    }

    http    = {
      source        = "hashicorp/http"
      version       = "3.2.1"
    }

    null    = {
      source        = "hashicorp/null"
      version       = "3.2.1"
    }

    time    = {
      source        = "hashicorp/time"
      version       = "0.9.1"
    }

    random  = {
      source        = "hashicorp/random"
      version       = "3.4.3"
    }

    kubernetes = {
      source        = "hashicorp/kubernetes"
      version       = "2.16.1"
    }

    kubectl = {
      source        = "gavinbunney/kubectl"
      version       = "1.14.0"
    }

    helm = {
      source        = "hashicorp/helm"
      version       = "2.8.0"
    }

    external = {
      source        = "hashicorp/external"
      version       = "2.2.3"
    }

    tls = {
      source        = "hashicorp/tls"
      version       = "4.0.4"
    }
  }

  required_version  = ">= 1.1.9"
}


provider "yandex" {

  token = var.token
}

provider "kubernetes" {
  host                   = module.k8s.cluster.master[0].external_v4_endpoint
  cluster_ca_certificate = module.k8s.cluster.master[0].cluster_ca_certificate

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "/root/yandex-cloud/bin/yc"
    args = [
      "k8s", "create-token",
      "--cloud-id", lookup(module.clouds.clouds,var.k8s_config.cloud_name,null).id,
      "--folder-id",lookup(module.folders.folders,var.k8s_config.folder_name,null).id,
      "--token", var.token,
    ]
  }
}

provider "helm" {

  kubernetes {
    host                   = module.k8s.cluster.master[0].external_v4_endpoint
    cluster_ca_certificate = module.k8s.cluster.master[0].cluster_ca_certificate

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "/root/yandex-cloud/bin/yc"
      args = [
        "k8s", "create-token",
        "--cloud-id", lookup(module.clouds.clouds,var.k8s_config.cloud_name,null).id,
        "--folder-id",lookup(module.folders.folders,var.k8s_config.folder_name,null).id,
        "--token", var.token,
      ]
    }
  }
}

provider "kubectl" {
  host                   = module.k8s.cluster.master[0].external_v4_endpoint
  cluster_ca_certificate = module.k8s.cluster.master[0].cluster_ca_certificate
  load_config_file       = false

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "/root/yandex-cloud/bin/yc"
    args = [
      "k8s", "create-token",
      "--cloud-id", lookup(module.clouds.clouds,var.k8s_config.cloud_name,null).id,
      "--folder-id",lookup(module.folders.folders,var.k8s_config.folder_name,null).id,
      "--token", var.token,
    ]
  }
}
