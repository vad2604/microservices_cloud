data "http" "yc_get_iam_token" {
  # получение iam-токена
  url = var.yc_iam_tokens_api
  method = "POST"

  request_body = jsonencode({
    yandexPassportOauthToken = "${var.token}"
  })

  request_headers = {
    Content-Type  = "application/json"
    Accept        = "application/json"
  }  
}
