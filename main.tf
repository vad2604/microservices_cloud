module "clouds" {
  # модуль создающий облако в нашей организации
  source = "./modules/clouds"

  organization_id = var.organization_id
  iam_token       = jsondecode(data.http.yc_get_iam_token.response_body).iamToken
  clouds_config   = var.clouds_config

  depends_on = [
    data.http.yc_get_iam_token
  ]
}

module "folders" {
  # модуль создающий фолдер в созданном облаке
  source = "./modules/folders"

  folders_config = var.folders_config

  depends_on = [
    module.clouds,
  ]
}

module "dns_zones" {
  # модуль создающий публичную dns-зону
  source = "./modules/dns-zones"

  iam_token         = jsondecode(data.http.yc_get_iam_token.response_body).iamToken
  dns_zones_config  = var.dns_zones_config

  depends_on = [
    module.folders,
  ]
}

module "networks" {
  # модуль создающий сети
  source = "./modules/networks"

  networks_config = var.networks_config

  depends_on = [
    module.dns_zones,
  ]
}

module "buckets" {
  # модуль создающий фолдер в созданном облаке
  source = "./modules/buckets"

  buckets_config = var.buckets_config

  depends_on = [
    module.folders,
  ]
}

module "logging_groups" {
  # модуль создающий лог-группу
  source = "./modules/logging"

  cloud_logging_config  = var.cloud_logging_config

  depends_on = [
    module.folders,
  ]
}

module "psql" {
  # модуль создающий кластер psql
  source      = "./modules/psql"

  psql_config = var.psql_config

  depends_on  = [
    module.networks
  ]
}

module "mongodb" {
  # модуль создающий кластер mongodb
  source        = "./modules/mongo"
  
  mongo_config  = var.mongo_config

  depends_on    = [
    module.networks
  ]
}

module "kafka" {
  # модуль создающий кластер mongodb
  source        = "./modules/kafka"

  kafka_config  = var.kafka_config

  depends_on    = [
    module.networks
  ]
}

module "k8s" {
  # модуль создающий кластеры k8s
  source      = "./modules/k8s"

  k8s_config  = var.k8s_config
  yc_token    = var.token

  depends_on  = [
    module.networks
  ]
}

module "fluent_bit" {
  # модуль создающий кластеры k8s
  source   = "./modules/fluent-bit"

  fluent_bit_config = var.fluent_bit_config
  
  kafka_password    = lookup(

    lookup(

      module.kafka.user_passwords, 
      var.fluent_bit_config.kafka_cluster_name, 
      null
    ), 
    var.fluent_bit_config.kafka_user, 
    null
  )

  depends_on = [
    module.k8s,
    module.kafka,
    module.logging_groups,
  ]
}

module "gitlab_runner" {
  # модуль создающий gitlab-runner в k8s
  source          = "./modules/gitlab-runner"
  
  gitlab_runner_config  = local.gitlab_runner_config

  depends_on = [
    module.k8s,
  ]
}

module "cert_manager" {
  # модуль создающий менеджер ssl-сертификатов в k8s
  source        = "registry.tfpla.net/terraform-iaac/cert-manager/kubernetes"

  cluster_issuer_email                   = var.cert_manager_config.email
  cluster_issuer_name                    = coalesce(var.cert_manager_config.issuer, "letsencrypt")
  cluster_issuer_private_key_secret_name = coalesce(var.cert_manager_config.private_key_secret_name, "cert-manager-private-key")
  cluster_issuer_server                  = coalesce(var.cert_manager_config.provider_url, "https://acme-v02.api.letsencrypt.org/directory")

  additional_set = [
    {
      name = "global.rbac.create"
      value = "true"
    },
    {
      name = "installCRDs"
      value = "true"
    },
  ]
  
  solvers = [
    {
      http01 = {
        ingress = {
          class = "nginx"
        }
      }
    }
  ]

  depends_on = [
    module.k8s,
  ]
}

module "ingress" {
  # модуль создающий игресс-контроллер в k8s
  source          = "./modules/ingress-nginx"
  
  ingress_config  = var.ingress_config

  depends_on = [
    module.cert_manager,
    module.dns_zones,
  ]
}
