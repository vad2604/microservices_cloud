# Terraform-план для развертывания инфраструктуры в Yandex Cloud

## Пошаговая инструкция
1. Получить OAuth-токен (https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token)
2. Получить ID организации (https://org.cloud.yandex.ru/settings)
3. Установить Terraform (https://developer.hashicorp.com/terraform/downloads?product_intent=terraform)
4. Установить Yandex Cloud CLI (https://cloud.yandex.ru/docs/cli/quickstart)
5. Настроить зеракало Terraform (https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)
6. Получить деплоймент-токен в Gitlab
7. Ввести в окружение переменные *TF_VAR_token*, *TF_VAR_organization_id*, *TF_VAR_runner_tokens*, *TF_VAR_pull_secret_passwords*.
```
    Пример:

      export TF_VAR_token=XXXXXXxxxxxxxXXXXXXxxxxxxxxx
      export TF_VAR_organization_id=xxxxxxxxxxxxxxxxx
      export TF_VAR_runner_tokens='{"runner-1":"XXxxxxxxxXXxxxxxx","runner-2":"XXXXXXxxxxxXxxxxx"}'
      export TF_VAR_pull_secret_passwords='{"test-1":"xxxxxxxxx","gitlab-registry":"xxxxxxxxxx"}'
```