resource "tls_private_key" "this" {

  for_each    = var.k8s_config.node_groups

  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "yandex_kubernetes_node_group" "this" {

  for_each    = var.k8s_config.node_groups

  description = each.value.description

  cluster_id  = yandex_kubernetes_cluster.this.id
  name        = each.key
  version     = each.value.k8s_node_version
  node_labels = coalesce(each.value.node_labels, var.defaults.node_groups.node_labels)
  node_taints = each.value.node_taints == null ? [] : each.value.node_taints

  instance_template {

    platform_id = coalesce(each.value.instance_template_platform_id, var.defaults.node_groups.instance_template_platform_id)

    resources {

      memory = each.value.resources_memory
      cores  = each.value.resources_cpu_cores
    }

    boot_disk {

      type = each.value.boot_disk_type
      size = each.value.boot_disk_size
    }

    network_interface {

      # выбор идентификаторов подсетей
      subnet_ids = concat(
      
        # для регионального кластера
        [
          for subnet in var.k8s_config.subnet_names: data.yandex_vpc_subnet.this[subnet].id
          if var.k8s_config.zonal_regional == "regional"
        ], 
        
        # для зонального кластера
        [
          for x in ["1"] : data.yandex_vpc_subnet.this[var.k8s_config.subnet_names[0]].id
          if var.k8s_config.zonal_regional == "zonal"        
        ]
      )

      security_group_ids = [
        yandex_vpc_security_group.this.id,
      ]
    }

    network_acceleration_type = (each.value.network_acceleration == null ? "standard" : "software_accelerated")

    scheduling_policy {

      preemptible = each.value.preemptible
    }

    metadata = {

      ssh-keys           = tls_private_key.this[each.key].public_key_openssh
      serial-port-enable = 1
    }
  }

  scale_policy {

    dynamic "auto_scale" {

      for_each = var.k8s_config.zonal_regional == "zonal" ? ["1"] : []

      content {
        initial = each.value.current_node_count
        min     = each.value.auto_scale_min_count
        max     = each.value.auto_scale_max_count
      }
    }

    dynamic "fixed_scale" {

      for_each = var.k8s_config.zonal_regional == "regional" ? ["1"] : []

      content {
        size = each.value.current_node_count
      }
    }
  }

  allocation_policy {

    dynamic "location" {

      for_each = var.k8s_config.zonal_regional == "regional" ? var.k8s_config.subnet_names : []

      content {
        zone = data.yandex_vpc_subnet.this[location.value].zone
      }
    }

    dynamic "location" {

      for_each = var.k8s_config.zonal_regional == "zonal" ? ["1"] : []

      content {
        zone      = data.yandex_vpc_subnet.this[var.k8s_config.subnet_names[0]].zone
      }
    }
  }

  maintenance_policy {

    auto_upgrade = coalesce(each.value.maintenance_policy_auto_upgrade, var.defaults.node_groups.maintenance_policy_auto_upgrade)
    auto_repair  = coalesce(each.value.maintenance_policy_auto_repair, var.defaults.node_groups.maintenance_policy_auto_repair)

    maintenance_window {

      day        = each.value.maintenance_window_day
      start_time = each.value.maintenance_window_start_time
      duration   = each.value.maintenance_window_duration
    }
  }

  labels = each.value.labels

  depends_on = [
    yandex_kubernetes_cluster.this,
  ]
}
