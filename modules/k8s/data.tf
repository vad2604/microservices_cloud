data "yandex_resourcemanager_cloud" "this" {
  name      = var.k8s_config.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  name      = var.k8s_config.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this.id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "yandex_vpc_network" "this" {
  name      = var.k8s_config.network_name
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_vpc_subnet" "this" {
  for_each = toset(var.k8s_config.subnet_names)

  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_vpc_subnet" "access_22_subnets" {
  for_each = toset(var.k8s_config.access_22_subnets)

  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_vpc_subnet" "access_API_subnets" {
  for_each = toset(var.k8s_config.access_API_subnets)

  name      = each.value
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}
