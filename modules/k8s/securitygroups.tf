resource "yandex_vpc_security_group" "this" {

  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените ее к кластеру и группам узлов."
  
  name        = "${var.k8s_config.name}-main-sg"
  folder_id   = data.yandex_resourcemanager_folder.this.id
  network_id  = data.yandex_vpc_network.this.id

  ingress {
    description    = "Правило разрешает проверки доступности с диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."

    protocol       = "TCP"
    v4_cidr_blocks = ["198.18.235.0/24", "198.18.248.0/24"]
    from_port      = 0
    to_port        = 65535
  }

  ingress {
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."

    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  ingress {

    description    = "Правило разрешает доступ по порту 22 (SSH)."
    protocol       = "TCP"    
    port           = 22
    
    v4_cidr_blocks = distinct(flatten([
        
        
        [ for subnet in data.yandex_vpc_subnet.access_22_subnets : subnet.v4_cidr_blocks ], 

        var.k8s_config.access_22_cidrs
    ]))
  }

  ingress {
    
    description    = "Правило разрешает подключение к API Kubernetes через порт 6443."
    protocol       = "TCP"      
    port           = 6443
    
    v4_cidr_blocks = distinct(flatten([
        
        [ for subnet in data.yandex_vpc_subnet.this : subnet.v4_cidr_blocks ], 

        [ for subnet in data.yandex_vpc_subnet.access_API_subnets : subnet.v4_cidr_blocks ], 

        var.k8s_config.access_API_cidrs
    ]))
  }

  ingress {
    
    description    = "Правило разрешает подключение к API Kubernetes через порт 443."
    protocol       = "TCP"      
    port           = 443
    
    v4_cidr_blocks = distinct(flatten([
        
        [ for subnet in data.yandex_vpc_subnet.this : subnet.v4_cidr_blocks ], 

        [ for subnet in data.yandex_vpc_subnet.access_API_subnets : subnet.v4_cidr_blocks ], 

        var.k8s_config.access_API_cidrs
    ]))
  }

  ingress {
    
    description    = "Правило разрешает подключение к игрессу через порт HTTPS. Порт настроен в игресс-контроллере"
    protocol       = "TCP"      
    port           = 32443
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    
    description    = "Правило разрешает подключение к игрессу через порт HTTP. Порт настроен в игресс-контроллере"
    protocol       = "TCP"      
    port           = 32080
    v4_cidr_blocks = ["0.0.0.0/0"]
  }


  dynamic "ingress" {
    for_each = var.k8s_config.access_ingress_whitelist

    content {

      description = "Доступ по портам ${ingress.value.protocol} ${ingress.value.from_port}-${ingress.value.to_port} до k8s"

      protocol       = ingress.value.protocol
      v4_cidr_blocks = ingress.value.v4_cidr_blocks
      from_port      = ingress.value.from_port
      to_port        = ingress.value.to_port
    }
  }

  egress {
    description    = "Правило разрешает весь исходящий трафик"

    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }

  depends_on = [
    data.yandex_resourcemanager_folder.this,
    data.yandex_vpc_network.this
  ]
}