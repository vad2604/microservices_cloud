resource "yandex_kms_symmetric_key" "this" {

  description = "Ключ шифрования секретов k8s-кластера ${var.k8s_config.name}"
  
  name              = "${var.k8s_config.name}-k8s-kms-key"
  folder_id         = data.yandex_resourcemanager_folder.this.id
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
}

