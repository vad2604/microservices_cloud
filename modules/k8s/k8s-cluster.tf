resource "yandex_kubernetes_cluster" "this" {

  description = var.k8s_config.description

  name       = var.k8s_config.name
  folder_id  = data.yandex_resourcemanager_folder.this.id
  network_id = data.yandex_vpc_network.this.id

  master {

    public_ip = var.k8s_config.public_ip

    security_group_ids = [
      yandex_vpc_security_group.this.id,
    ]

    dynamic "regional" {

      for_each = var.k8s_config.zonal_regional == "regional" ? ["1"] : []

      content {

        region = coalesce(var.k8s_config.region, var.defaults.cluster.region)

        dynamic "location" {

          for_each = var.k8s_config.subnet_names

          content {

            subnet_id = data.yandex_vpc_subnet.this["${location.value}"].id
            zone      = data.yandex_vpc_subnet.this["${location.value}"].zone
          }
        }
      }
    }

    dynamic "zonal" {

      for_each = var.k8s_config.zonal_regional == "zonal" ? ["1"] : []

      content {
        subnet_id = data.yandex_vpc_subnet.this[var.k8s_config.subnet_names[0]].id
        zone      = data.yandex_vpc_subnet.this[var.k8s_config.subnet_names[0]].zone
      }
    }

    version = var.k8s_config.k8s_master_version

    maintenance_policy {

      auto_upgrade = coalesce(var.k8s_config.master_auto_upgrade, var.defaults.cluster.master_auto_upgrade)

      maintenance_window {

        day        = var.k8s_config.maintenance_window_day
        start_time = var.k8s_config.maintenance_window_start_time
        duration   = var.k8s_config.maintenance_window_duration
      }
    }
  }

  kms_provider {

    key_id = yandex_kms_symmetric_key.this.id
  }

  cluster_ipv4_range       = var.k8s_config.cluster_ipv4_range
  service_ipv4_range       = var.k8s_config.service_ipv4_range
  service_account_id       = yandex_iam_service_account.cluster.id
  node_service_account_id  = yandex_iam_service_account.nodes.id
  release_channel          = coalesce(var.k8s_config.release_channel, var.defaults.cluster.release_channel)
  network_policy_provider  = var.k8s_config.network_policy_provider == null ? "CALICO" : var.k8s_config.network_policy_provider == "CILIUM" ? null : var.k8s_config.network_policy_provider

  dynamic "network_implementation" {
    for_each = var.k8s_config.network_policy_provider == "CILIUM" ? ["1"] : []

    content {
      cilium {}
    }
  }

  labels = var.k8s_config.labels

  depends_on = [
    yandex_resourcemanager_folder_iam_member.cluster,
    yandex_resourcemanager_folder_iam_member.nodes,
    yandex_kms_symmetric_key.this,
    data.yandex_vpc_subnet.this,
    yandex_vpc_security_group.this,
  ]
}

resource "kubernetes_namespace" "this" {

  for_each = toset(var.k8s_config.ns_create_list)

  metadata {
    annotations = {
      name = each.value
    }

    name = each.value
  }

  depends_on = [
    yandex_kubernetes_cluster.this
  ]
}
