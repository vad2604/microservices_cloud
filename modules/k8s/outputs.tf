output "cluster" {
  value = yandex_kubernetes_cluster.this
}

output "node_groups" {
  value = yandex_kubernetes_node_group.this
}

output "ssh_keys" {
  value = tls_private_key.this
}
