resource "yandex_iam_service_account" "cluster" {
  description = "Сервисный аккаунт k8s для управления ВМ"

  name      = "${var.k8s_config.name}-cluster-sa"
  folder_id = data.yandex_resourcemanager_folder.this.id
}

resource "yandex_iam_service_account" "nodes" {
  description = "Сервисный аккаунт k8s-nodes (например, для пуша образов в Container Registry)"

  name      = "${var.k8s_config.name}-node-groups-sa"
  folder_id = data.yandex_resourcemanager_folder.this.id
}

resource "yandex_resourcemanager_folder_iam_member" "cluster" {
  for_each = toset(var.k8s_config.cluster_sa_roles)

  role      = each.key
  member    = "serviceAccount:${yandex_iam_service_account.cluster.id}"
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    yandex_iam_service_account.cluster,
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "nodes" {
  for_each = toset(var.k8s_config.node_groups_sa_roles)

  role      = each.key
  member    = "serviceAccount:${yandex_iam_service_account.nodes.id}"
  folder_id = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    yandex_iam_service_account.nodes,
  ]
}
