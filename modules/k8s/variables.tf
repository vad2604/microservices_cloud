variable "yc_token" {
  description = "OAuth-токен для Yandex Cloud"

  type = string
}

variable "defaults" {
  description = "Список дефолтных переменных и их значений"

  type = object({

    cluster = object({

      region                   = string
      master_auto_upgrade      = bool
      release_channel          = string
      network_policy_provider  = string
    })

    node_groups = object({

      instance_template_platform_id   = string
      maintenance_policy_auto_upgrade = bool
      maintenance_policy_auto_repair  = bool
      preemptible                     = bool
      node_labels                     = map(string)
    })
  })

  default = {

    cluster = {

      region                   = "ru-central1"
      master_auto_upgrade      = false
      release_channel          = "REGULAR"
      network_policy_provider  = "CALICO"
    }

    node_groups = {

      instance_template_platform_id   = "standard-v3"
      maintenance_policy_auto_upgrade = false
      maintenance_policy_auto_repair  = true
      preemptible                     = false
      node_labels                     = {}
    }
  }
}


variable "k8s_config" {
  description = "Переменная для конфигурирования кластеров и их node groups"

  type = object({
    description: string
    name: string

    cloud_name   : string
    folder_name  : string
    network_name : string

    ns_create_list: list(string) # список неймспейсов для создания

    zonal_regional: string # в зональной конфигурации возьмётся зона первой подсети
    cluster_sa_roles: list(string)
    node_groups_sa_roles: list(string)
    k8s_master_version: string
    maintenance_window_day: string
    maintenance_window_start_time: string
    maintenance_window_duration: string
    prevent_destroy: bool
    
    public_ip: bool
    cluster_ipv4_range: string
    service_ipv4_range: string
    subnet_names: list(string) # в зональной конфигурации возьмётся зона первой подсети
    access_22_cidrs: list(string)
    access_API_cidrs: list(string)
    access_22_subnets: list(string)
    access_API_subnets: list(string)

    access_ingress_whitelist: map(object({
      description: string

      v4_cidr_blocks: list(string)
      from_port: number
      to_port: number
      protocol: string
    }))

    labels: map(string)

    region: optional(string)
    master_auto_upgrade: optional(bool)
    release_channel: optional(string)
    network_policy_provider: optional(string)

    node_groups: map(object({
      # Ключ - имя node group
      description: string

      #subnet_name: list(string) # в зональной конфигурации возьмётся зона первой подсети
      k8s_node_version: string
      resources_memory: number
      resources_cpu_cores: number
      boot_disk_type: string
      boot_disk_size: number
      current_node_count: number
      auto_scale_min_count: optional(number)
      auto_scale_max_count: optional(number)
      maintenance_window_day: string
      maintenance_window_start_time: string
      maintenance_window_duration: string
      node_taints: optional(list(string))
      network_acceleration: optional(bool)

      labels: map(string)

      instance_template_platform_id: optional(string)
      maintenance_policy_auto_upgrade: optional(bool)
      maintenance_policy_auto_repair: optional(bool)
      preemptible: optional(bool)
      node_labels: optional(map(string))
    }))
    
  })
}