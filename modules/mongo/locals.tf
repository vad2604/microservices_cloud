locals {

  mongo_config_full = { 
    for mongo_key, mongo_config in var.mongo_config : mongo_key => merge(var.defaults, mongo_config)
  }

  subnets = {

    for entry in distinct(flatten([

      for mongo_key, mongo_config in local.mongo_config_full : [

        for subnet in mongo_config.subnet_name : {

          mongo_key : mongo_key
          subnet    : subnet
        }
      ]
    ])) : "${entry.mongo_key}.${entry.subnet}" => entry
  }

  access_27018_subnets = {

    for entry in distinct(flatten([

      for mongo_key, mongo_config in local.mongo_config_full : [

        for subnet in mongo_config.access_27018_subnets : {

          mongo_key : mongo_key
          subnet    : subnet
        }
      ]
    ])) : "${entry.mongo_key}.${entry.subnet}" => entry
  }

  users = {

    for entry in distinct(flatten([

      for mongo_key, mongo_config in local.mongo_config_full : [

        for user_key, user_config in mongo_config.users : {

          mongo_key             : mongo_key
          user_key              : user_key
          accessible_databases  : user_config.accessible_databases
        }
      ]
    ])) : "${entry.mongo_key}.${entry.user_key}" => entry
  }
}