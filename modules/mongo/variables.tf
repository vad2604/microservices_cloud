variable "defaults" {
  description = "Значения по-умолчанию для кластера MongoDB"

  type = object({

    environment = string
    version     = string
  })

  default = {

    environment = "PRODUCTION"
    version     = "5.0"
  }
}

variable "mongo_config" {
  description = "Переменная, описывающая конфигурацию кластеров MongoDB"

  type = map(object({
    
    description = string

    cloud_name    = string
    folder_name   = string
    network_name  = string
    environment   = optional(string)
    version       = optional(string)
    deletion_protection = bool
    public_access = bool

    subnet_name          = list(string)
    access_27018_subnets = list(string)
    access_27018_cidrs   = optional(list(string))

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = optional(object({

      type = string
      day  = string
      hour = number
    }))
    
    databases = list(string)

    users = map(object({

      accessible_databases = set(object({
        database_name = string
        
        # https://cloud.yandex.com/en-ru/docs/managed-mongodb/concepts/users-and-roles
        roles         = list(string)
      }))
    }))

    labels      = map(string)
    
  }))

}
