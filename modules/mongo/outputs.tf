output "user_passwords" {
  value = {

    for mongo_key, mongo_config in local.mongo_config_full: 

    mongo_key => {

      for user in local.users: user.user_key => "${random_password.user_passwords["${user.mongo_key}.${user.user_key}"].result}"
      if user.mongo_key == mongo_key
    }
  }
  sensitive = true
}

output "clusters" {
  value = yandex_mdb_mongodb_cluster.this
  sensitive = true
}
