resource "random_password" "user_passwords" {

  for_each    = local.users

  length      = 20
  special     = false
  min_lower   = 3
  min_upper   = 3
}

resource "yandex_mdb_mongodb_cluster" "this" {

  for_each = local.mongo_config_full

  description = each.value.description

  name        = each.key
  environment = coalesce(each.value.environment, var.defaults.environment)
  network_id  = data.yandex_vpc_network.this[each.key].id
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id

  cluster_config {
    version = coalesce(each.value.version, var.defaults.version)

    backup_window_start {

      hours   = each.value.backup_window_start.hours
      minutes = each.value.backup_window_start.minutes
    }
  }

  labels = each.value.labels


  dynamic "database" {

    for_each = each.value.databases

    content {
      
      name  = database.value
    }
  }
  
  dynamic "user" {

    for_each = each.value.users
    
    content {
      
      name     = user.key
      password = random_password.user_passwords["${each.key}.${user.key}"].result

      dynamic "permission" {
        for_each = user.value.accessible_databases

        content {
          database_name = permission.value.database_name
          roles         = permission.value.roles
        }
      }

    }
  }

  resources {
    resource_preset_id = each.value.resource_preset_id
    disk_type_id       = each.value.disk_type_id
    disk_size          = each.value.disk_size
  }

  dynamic "host" {
    for_each = each.value.subnet_name
    
    content {

        zone_id          = data.yandex_vpc_subnet.this["${each.key}.${host.value}"].zone
        subnet_id        = data.yandex_vpc_subnet.this["${each.key}.${host.value}"].id
        assign_public_ip = coalesce(each.value.public_access, true)
    }
  }

  dynamic "maintenance_window" {
    for_each = {
      for x in ["1"] : x => x
      if each.value.maintenance_window != null
    }

    content {
      type = each.value.maintenance_window.type
      day  = each.value.maintenance_window.day
      hour = each.value.maintenance_window.hour
    }
  }

  security_group_ids = [
   
    yandex_vpc_security_group.this[each.key].id
  ]
  
  deletion_protection = each.value.deletion_protection

  depends_on = [
    yandex_vpc_security_group.this,
    random_password.user_passwords,
    data.yandex_vpc_subnet.access_27018_subnets,
    data.yandex_vpc_subnet.this
  ]
}