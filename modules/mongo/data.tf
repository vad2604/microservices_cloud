data "yandex_resourcemanager_cloud" "this" {
  for_each  = local.mongo_config_full

  name      = each.value.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  for_each  = local.mongo_config_full

  name      = each.value.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "yandex_vpc_network" "this" {
  for_each  = local.mongo_config_full

  name      = each.value.network_name
  folder_id = data.yandex_resourcemanager_folder.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_vpc_subnet" "this" {
  for_each = local.subnets

  name      = each.value.subnet
  folder_id = data.yandex_resourcemanager_folder.this[each.value.mongo_key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}


data "yandex_vpc_subnet" "access_27018_subnets" {
  for_each = local.access_27018_subnets

  name      = each.value.subnet
  folder_id = data.yandex_resourcemanager_folder.this[each.value.mongo_key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}
