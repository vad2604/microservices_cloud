locals {

  cloud_users_roles = {

    for index, entry in distinct(flatten([

      for cloud_key, cloud_config in var.clouds_config : [

        for user_email, cloud_user_spec in cloud_config.cloud_users : [

          for cloud_user_role in cloud_user_spec.cloud_user_roles: {

            cloud_key       : cloud_key
            cloud_user_email: user_email
            cloud_user_role : cloud_user_role
          }
        ]
      ]
    ])) : "${entry.cloud_key}.${entry.cloud_user_email}.${entry.cloud_user_role}" => entry   
  }

  cloud_users = {

    for index, entry in distinct(flatten([

      for cloud_key, cloud_config in var.clouds_config : [

        for user_email, cloud_user_spec in cloud_config.cloud_users : {

          cloud_key       : cloud_key
          cloud_user_email: user_email
        }
      ]
    ])) : "${entry.cloud_key}.${entry.cloud_user_email}" => entry.cloud_user_email 
  }

}