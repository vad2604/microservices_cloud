data "http" "yc_billing_accounts" {
  # получение биллинг аккаунта
  url = var.yc_billing_accounts_api
  method = "GET"

  request_headers = {
    Content-Type  = "application/json"
    Accept        = "application/json"
    Authorization = "Bearer ${var.iam_token}"
  }
}

data "http" "link_yc_billing_account_to_clouds" {
  for_each = var.clouds_config
  # связывание созданных облаков с биллинг аккаунтом 0
  url = "${var.yc_billing_accounts_api}/${jsondecode(data.http.yc_billing_accounts.response_body).billingAccounts.0.id}/billableObjectBindings"
  method = "POST"

  request_body = jsonencode({
    billableObject = { 
      id = "${yandex_resourcemanager_cloud.this["${each.key}"].id}", 
      type = "cloud" 
    }
  })

  request_headers = {
    Content-Type  = "application/json"
    Accept        = "application/json"
    Authorization = "Bearer ${var.iam_token}"
  }

  depends_on = [
    yandex_resourcemanager_cloud.this
  ]
}

data "yandex_iam_user" "this" {
  # получение iam-аккаунтов по указанным в конфигурациях облаков почтовым ящикам

  for_each = local.cloud_users

  login = each.value
}
