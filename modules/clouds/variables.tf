variable "organization_id" {
  description = "ID организации"

  type        = string
}

variable "iam_token" {
  description = "IAM-токен владельца организации"

  type        = string
  sensitive   = true
}

variable "clouds_config" {
  description = "Описание облаков, которые создаются/управляются из terraform"

  # определение типа для данной переменной
  type = map(object({
    # ключ = имя облака
    description: string

    labels: map(string)

    cloud_users: map(object({
      cloud_user_roles: list(string)
    }))

  }))
}

variable "yc_billing_accounts_api" {
  description = "API YCloud по управлению платежными аккаунтами"

  type    = string
  default = "https://billing.api.cloud.yandex.net/billing/v1/billingAccounts"
}

