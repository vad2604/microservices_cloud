resource "yandex_resourcemanager_cloud" "this" {
  for_each = var.clouds_config
  # на основании конфигурации облака создание облака
  description = each.value.description
  name        = each.key

  labels = each.value.labels

  organization_id = var.organization_id

  depends_on = [
    data.http.yc_billing_accounts,
  ]
}

resource "time_sleep" "wait" {
  # после создания облака выжидаем 15 секунд, перед созданием объектов внутри облака 
  create_duration = "15s"

  depends_on = [
    yandex_resourcemanager_cloud.this,
    data.http.link_yc_billing_account_to_clouds
  ]
}

resource "yandex_resourcemanager_cloud_iam_member" "this" {
  # основываясь на конфигурации облака назначаем роли указанным пользователям
  for_each = local.cloud_users_roles

  cloud_id = yandex_resourcemanager_cloud.this[each.value.cloud_key].id
  role     = each.value.cloud_user_role

  member = "userAccount:${data.yandex_iam_user.this["${each.value.cloud_key}.${each.value.cloud_user_email}"].id}"

  depends_on = [
    yandex_resourcemanager_cloud.this,
    local.cloud_users_roles
  ]
}
