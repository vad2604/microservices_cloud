resource "yandex_vpc_gateway" "this" {

  for_each = var.networks_config

  description = each.value.gateway_description
  name        = "${each.key}-gateway"
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id

  shared_egress_gateway {}

  depends_on = [
    yandex_vpc_network.this,
  ]
}

resource "yandex_vpc_route_table" "this" {

  for_each = var.networks_config

  description = each.value.route_table_description
  network_id  = yandex_vpc_network.this[each.key].id
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.this[each.key].id
  }

  depends_on = [
    yandex_vpc_network.this,
    yandex_vpc_gateway.this,
  ]
}
