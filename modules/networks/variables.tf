variable "networks_config" {
  description = "Конфигурация сети инфраструктуры"
  
  type = map(object({    
    # Ключ - имя сети
    description: string

    cloud_name: string
    folder_name: string
    gateway_description: string
    route_table_description: string

    subnets: map(object({
      # Ключ - имя подсети
      v4_cidr_blocks: list(string)
      availability_zone: string
      use_nat: bool
    }))

    labels: map(string)

  }))

}
