output "networks" {
    description = "Вывод информации о сетях"

    value = yandex_vpc_network.this
}

output "subnets" {
    description = "Вывод информации о подсетях"

    value = yandex_vpc_subnet.this
}

output "gateways" {
    description = "Вывод информации о подсетях"

    value = yandex_vpc_gateway.this
}

output "route_tables" {
    description = "Вывод информации о подсетях"

    value = yandex_vpc_route_table.this
}