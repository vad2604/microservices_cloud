resource "yandex_vpc_network" "this" {

  for_each = var.networks_config

  description = each.value.description

  name      = each.key
  folder_id = data.yandex_resourcemanager_folder.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

resource "yandex_vpc_subnet" "this" {

  for_each = local.network_subnets

  name           = each.value.subnet_key
  v4_cidr_blocks = each.value.subnet_config.v4_cidr_blocks
  zone           = each.value.subnet_config.availability_zone
  network_id     = yandex_vpc_network.this[each.value.network_key].id
  folder_id      = data.yandex_resourcemanager_folder.this[each.value.network_key].id
  route_table_id = each.value.subnet_config.use_nat ? yandex_vpc_route_table.this[each.value.network_key].id : null

  depends_on = [
    yandex_vpc_network.this,
    yandex_vpc_gateway.this,
    yandex_vpc_route_table.this,
  ]
}
