locals {

  network_subnets = {

    for entry in distinct(flatten([

      for network_key, network_config in var.networks_config : [

        for subnet_key, subnet_config in network_config.subnets : {

          network_key   : network_key
          subnet_key    : subnet_key
          subnet_config : subnet_config
        }
      ]
    ])) : "${entry.network_key}.${entry.subnet_key}" => entry 
  }

}