resource "random_password" "user_passwords" {
  for_each          = local.users

  length            = 20
  special           = false
  min_lower         = 3
  min_upper         = 3
}

resource "random_password" "admin_password" {
  
  for_each          = local.psql_config_full

  length            = 20
  special           = false
  min_lower         = 3
  min_upper         = 3
}

resource "yandex_mdb_postgresql_cluster" "this" {
  
  for_each          = local.psql_config_full

  description = each.value.description
  
  name        = each.key
  network_id  = data.yandex_vpc_network.this[each.key].id
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id
  environment = coalesce(each.value.environment, var.defaults.environment)
  
  
  security_group_ids = [
    yandex_vpc_security_group.this[each.key].id
  ]
 

  config {
    version = coalesce(each.value.version, var.defaults.version)

    resources {
      resource_preset_id = each.value.resource_preset_id
      disk_type_id       = each.value.disk_type_id
      disk_size          = each.value.disk_size
    }

    postgresql_config = {
      max_connections                   = coalesce(each.value.max_connections, var.defaults.max_connections)
      enable_parallel_hash              = coalesce(each.value.enable_parallel_hash, var.defaults.enable_parallel_hash)
      vacuum_cleanup_index_scale_factor = coalesce(each.value.vacuum_cleanup_index_scale_factor, var.defaults.vacuum_cleanup_index_scale_factor)
      autovacuum_vacuum_scale_factor    = coalesce(each.value.autovacuum_vacuum_scale_factor, var.defaults.autovacuum_vacuum_scale_factor)
      default_transaction_isolation     = coalesce(each.value.default_transaction_isolation, var.defaults.default_transaction_isolation)
      shared_preload_libraries          = coalesce(each.value.shared_preload_libraries, var.defaults.shared_preload_libraries)
    }
  
    pooler_config {
      pool_discard = coalesce(each.value.pool_discard, var.defaults.pool_discard)
      pooling_mode = coalesce(each.value.pooling_mode, var.defaults.pooling_mode)
    }

    backup_window_start {
      hours   = each.value.backup_window_start.hours
      minutes = each.value.backup_window_start.minutes
    }
  }

  dynamic "maintenance_window" {
    for_each = {
      for x in ["1"] : x => x
      if each.value.maintenance_window != null
    }

    content {
      type = each.value.maintenance_window.type
      day  = each.value.maintenance_window.day
      hour = each.value.maintenance_window.hour
    }
  }

  dynamic "database" {
    for_each = each.value.databases

    content {
      lc_collate = coalesce(database.value.lc_collate, var.defaults.lc_collate)
      lc_type    = coalesce(database.value.lc_type, var.defaults.lc_type)
      name       = database.key
      owner      = database.value.owner

      dynamic "extension" {
        for_each = database.value.extension == null ? toset([]) : toset(database.value.extension)

        content {
          name = extension.value
        }
      }
    }
  }

  user {
    # учётная запись администратора
    name        = each.value.admin_name
    password    = random_password.admin_password[each.key].result
    conn_limit  = 100
    grants      = ["mdb_admin"]
  }

  dynamic "user" {
    for_each = each.value.users
    
    content {
      
      name       = user.key
      password   = random_password.user_passwords["${each.key}.${user.key}"].result
      conn_limit = coalesce(user.value.conn_limit, var.defaults.user_conn_limit)

      dynamic "permission" {
        for_each = user.value.accessible_databases

        content {
          database_name = permission.value
        }
      }

      grants = user.value.grants
    }
  }

  dynamic "host" {
    for_each = each.value.hosts

    content {

        name             = host.value.host_name
        priority         = host.value.priority
        zone             = data.yandex_vpc_subnet.this["${each.key}.${host.key}"].zone
        subnet_id        = data.yandex_vpc_subnet.this["${each.key}.${host.key}"].id
        assign_public_ip = coalesce(each.value.public_access, true)
        replication_source_name = host.value.replication_source_name
        replication_source      = host.value.replication_source
    }
  }

  deletion_protection = each.value.deletion_protection

  depends_on = [
    yandex_vpc_security_group.this,
    random_password.user_passwords,
    random_password.admin_password,
    data.yandex_vpc_subnet.access_6432_subnets,
    data.yandex_vpc_subnet.this
  ]
}
