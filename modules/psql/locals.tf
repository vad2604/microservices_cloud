locals {

  psql_config_full = { 
    for psql_key, psql_config in var.psql_config : psql_key => merge(var.defaults, psql_config)
  }

  host_subnet = {

    for entry in distinct(flatten([

      for psql_key, psql_config in local.psql_config_full : [

        for host_key, host_config in psql_config.hosts : {

          psql_key    : psql_key
          host_key    : host_key
          host_config : host_config
        }
      ]
    ])) : "${entry.psql_key}.${entry.host_key}" => entry
  }

  access_6432_subnets = {

    for entry in distinct(flatten([

      for psql_key, psql_config in local.psql_config_full : [

        for subnet in psql_config.access_6432_subnets : {

          psql_key : psql_key
          subnet   : subnet
        }
      ]
    ])) : "${entry.psql_key}.${entry.subnet}" => entry
  }

  users = {

    for entry in distinct(flatten([

      for psql_key, psql_config in local.psql_config_full : [

        for user_key, user_config in psql_config.users : {

          psql_key    : psql_key
          user_key    : user_key
          conn_limit  : user_config.conn_limit
          grants      : user_config.grants
          accessible_databases : user_config.accessible_databases
        }
      ]
    ])) : "${entry.psql_key}.${entry.user_key}" => entry
  }
}
