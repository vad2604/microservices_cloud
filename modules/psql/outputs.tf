output "user_passwords" {
  value = {

    for psql_key, psql_config in local.psql_config_full: 

    psql_key => {

      for user in local.users: user.user_key => "${random_password.user_passwords["${user.psql_key}.${user.user_key}"].result}"
      if user.psql_key == psql_key
    }
  }
  sensitive = true
}

output "admin_accounts" {
  value = {
    for psql_key1, psql_config1 in local.psql_config_full:
    psql_key1 => {
      
      for psql_key2, psql_config2 in local.psql_config_full: psql_config2.admin_name => "${random_password.admin_password["${psql_key2}"].result}"
      if psql_key1 == psql_key2
    }
  }
  sensitive = true
}

output "clusters" {
  value = yandex_mdb_postgresql_cluster.this
  sensitive = true
}
