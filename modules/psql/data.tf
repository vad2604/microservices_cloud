data "yandex_resourcemanager_cloud" "this" {
  for_each  = local.psql_config_full

  name      = each.value.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  for_each  = local.psql_config_full

  name      = each.value.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "yandex_vpc_network" "this" {
  for_each  = local.psql_config_full

  name      = each.value.network_name
  folder_id = data.yandex_resourcemanager_folder.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_vpc_subnet" "this" {
  for_each = local.host_subnet

  name      = each.value.host_config.subnet_name
  folder_id = data.yandex_resourcemanager_folder.this[each.value.psql_key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}


data "yandex_vpc_subnet" "access_6432_subnets" {
  for_each = local.access_6432_subnets
  
  name      = each.value.subnet
  folder_id = data.yandex_resourcemanager_folder.this[each.value.psql_key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}
