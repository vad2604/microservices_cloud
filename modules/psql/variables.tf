variable "defaults" {
  description = "Значения по-умолчанию для кластера"

  type = object({

    environment = string
    version     = string

    max_connections                   = number
    enable_parallel_hash              = bool
    vacuum_cleanup_index_scale_factor = number
    autovacuum_vacuum_scale_factor    = number
    default_transaction_isolation     = string
    shared_preload_libraries          = string
    
    pool_discard = bool
    public_access = bool
    pooling_mode = string

    lc_collate    = string
    lc_type       = string

    user_conn_limit = number
  })

  default = {

    environment = "PRODUCTION"
    version     = "14"

    max_connections                   = 395
    enable_parallel_hash              = true
    vacuum_cleanup_index_scale_factor = 0.2
    autovacuum_vacuum_scale_factor    = 0.34
    default_transaction_isolation     = "TRANSACTION_ISOLATION_READ_COMMITTED"
    shared_preload_libraries          = "SHARED_PRELOAD_LIBRARIES_AUTO_EXPLAIN,SHARED_PRELOAD_LIBRARIES_PG_HINT_PLAN"

    pool_discard = true
    public_access = true
    pooling_mode = "SESSION"

    lc_collate = "en_US.UTF-8"
    lc_type    = "en_US.UTF-8"

    user_conn_limit = 3
  }
}


variable "psql_config" {
  description = "Переменная, описывающая конфигурацию кластеров постгреса"

  type = map(object({
    
    description = string

    cloud_name          = string
    folder_name         = string
    network_name        = string
    admin_name          = string
    public_access       = optional(bool)

    hosts               = map(object({
      
      subnet_name             = string
      host_name               = optional(string)
      priority                = optional(number)
      replication_source_name = optional(string)
      replication_source      = optional(string)
    }))

    access_6432_subnets = list(string)
    access_6432_cidrs   = optional(list(string))

    environment = optional(string)
    version     = optional(string)

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number
    deletion_protection = bool
    user_conn_limit     = optional(number)

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = optional(object({

      type = string
      day  = optional(string)
      hour = optional(number)
    }))
    
    users = map(object({

      conn_limit           = optional(number)
      accessible_databases = list(string)
      grants               = optional(list(string))
    }))

    pool_discard = optional(bool)
    pooling_mode = optional(string)
     
    max_connections                   = optional(number)
    enable_parallel_hash              = optional(bool)
    vacuum_cleanup_index_scale_factor = optional(number)
    autovacuum_vacuum_scale_factor    = optional(number)
    default_transaction_isolation     = optional(string)
    shared_preload_libraries          = optional(string)

    labels = map(string)

    databases = map(object({
      # Конфигурация баз данных кластера psql
      lc_collate    = optional(string)
      lc_type       = optional(string)
      owner         = string
      extension     = list(string)
    }))

  }))
}

