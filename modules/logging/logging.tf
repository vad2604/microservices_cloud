resource "yandex_logging_group" "this" {

  for_each = var.cloud_logging_config

  folder_id         = data.yandex_resourcemanager_folder.this[each.key].id

  name              = each.key
  description       = coalesce(each.value.description, local.default_description)
  retention_period  = coalesce(each.value.retention_period, local.default_retention_period)
  labels            = coalesce(each.value.labels, local.default_labels)
}
