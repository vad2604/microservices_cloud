variable "cloud_logging_config" {
  description = "Конфигуация для сервиса logging в Yandex Cloud"

  type = map(object({
    # ключ = имя группы
    cloud_name       = string
    folder_name      = string
    description      = optional(string)
    labels           = optional(map(string))
    retention_period = optional(string)
  }))

}
