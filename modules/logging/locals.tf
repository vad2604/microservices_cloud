locals {
  ############
  # Defaults
  ############
  default_retention_period = "72h"
  default_description      = "Empty description"
  default_labels           = {}
}
