variable "folders_config" {
  description = "Описание фолдеров, которые создаются/управляются из terraform"
  
  # определение типа для данной переменной
  type = map(object({
    # ключ = имя фолдера
    description : string
    
    cloud_name: string
    labels: map(string)

    folder_users: map(object({
      folder_user_roles: list(string)
    }))
    
  }))
}
