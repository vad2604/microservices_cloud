resource "yandex_resourcemanager_folder" "this" {

  for_each = var.folders_config
  # на основании кофигураций фолдеров создание фолдеров
  description = each.value.description

  cloud_id = data.yandex_resourcemanager_cloud.this[each.key].id
  name     = each.key

  labels = each.value.labels

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "this" {
  # основываясь на конфигурации фолдера назначаем роли указанным пользователям

  for_each = local.folder_users_roles

  folder_id = yandex_resourcemanager_folder.this[each.value.folder_key].id

  member = "userAccount:${data.yandex_iam_user.this["${each.value.folder_key}.${each.value.folder_user_email}"].id}"
  role   = each.value.folder_user_role

  depends_on = [
    data.yandex_iam_user.this,
    yandex_resourcemanager_folder.this
  ]
}
