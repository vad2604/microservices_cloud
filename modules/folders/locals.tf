locals {

  folder_users_roles = {

    for index, entry in distinct(flatten([

      for folder_key, folder_config in var.folders_config : [

        for user_email, folder_user_spec in folder_config.folder_users : [

          for folder_user_role in folder_user_spec.folder_user_roles: {

            folder_key       : folder_key
            folder_user_email: user_email
            folder_user_role : folder_user_role
          }
        ]
      ]
    ])) : "${entry.folder_key}.${entry.folder_user_email}.${entry.folder_user_role}" => entry  
  }

  folder_users = {

    for index, entry in distinct(flatten([

      for folder_key, folder_config in var.folders_config : [

        for user_email, folder_user_spec in folder_config.folder_users : {

          folder_key       : folder_key
          folder_user_email: user_email
        }
      ]
    ])) : "${entry.folder_key}.${entry.folder_user_email}" => entry.folder_user_email 
  }

}