data "yandex_resourcemanager_cloud" "this" {
  for_each = var.folders_config

  name = each.value.cloud_name
}

data "yandex_iam_user" "this" {
  # получение iam-аккаунтов по указанным в конфигурациях фолдеров почтовым ящикам
  for_each = local.folder_users

  login = each.value
}
