resource "yandex_dns_zone" "this" {
  
  for_each = var.dns_zones_config

  name        = each.key
  description = each.value.description
  zone        = "${each.value.dns_zone}."
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id
  public      = each.value.public

  labels      = each.value.labels

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

resource "time_sleep" "this" {
  # после создания зоны выжидаем 5 секунд 
  create_duration = "5s"

  depends_on = [
    yandex_dns_zone.this,
  ]
}