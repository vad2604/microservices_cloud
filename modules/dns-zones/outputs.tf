output "dns_zones" {
  # выводим созданные облака как результат модуля
  value = yandex_dns_zone.this
}

output "ns_records" {
  # выводим список ns-записей нашей dns-зоны
  value = {
    for key, ns_records in data.http.dns_zone_ns_records : key => jsondecode(ns_records.response_body)
  }
}