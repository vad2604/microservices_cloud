variable "iam_token" {
  description = "IAM-токен владельца организации"

  type        = string
  sensitive   = true
}

variable "dns_zones_config" {
  # определение типа для данной переменной
  type = map(object({
    # ключ = имя зоны
    description: string
    dns_zone: string
    public: bool
    cloud_name: string
    folder_name: string

    labels: map(string)
  
  }))

}

variable "yc_dns_zones_api" {
  description = "API YCloud по управлению IAM-токенами"

  type    = string
  default = "https://dns.api.cloud.yandex.net/dns/v1/zones"
}
