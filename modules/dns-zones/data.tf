data "yandex_resourcemanager_cloud" "this" {
  for_each  = var.dns_zones_config

  name      = each.value.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  for_each  = var.dns_zones_config

  name      = each.value.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "http" "dns_zone_ns_records" {
  
  for_each = var.dns_zones_config
  # получение списка ns-записей нашей dns-зоны
  url = "${var.yc_dns_zones_api}/${yandex_dns_zone.this["${each.key}"].id}:getRecordSet?name=@&type=NS"
  method = "GET"

  request_headers = {
    Content-Type  = "application/json"
    Accept        = "application/json"
    Authorization = "Bearer ${var.iam_token}"
  }

  depends_on = [
    yandex_dns_zone.this,
    time_sleep.this
  ]
}
