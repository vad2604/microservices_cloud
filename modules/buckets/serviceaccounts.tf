
resource "yandex_iam_service_account" "this" {
  for_each = var.buckets_config
  
  description = "Сервис-аккаунт баккета ${each.key}-bucket"
  
  name      = "${each.key}-bucket-sa"
  folder_id = data.yandex_resourcemanager_folder.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}


resource "yandex_resourcemanager_folder_iam_member" "this" {
  for_each = var.buckets_config

  folder_id = data.yandex_resourcemanager_folder.this[each.key].id

  role   = "storage.admin"
  member = "serviceAccount:${yandex_iam_service_account.this[each.key].id}"

  depends_on = [
    yandex_iam_service_account.this
  ]
}


resource "yandex_iam_service_account_static_access_key" "this" {
  for_each = var.buckets_config
  
  description        = "Ключ доступа для Объектного хранилища ${each.key}-bucket"
  service_account_id = yandex_iam_service_account.this[each.key].id

  depends_on = [
    yandex_iam_service_account.this
  ]
}
