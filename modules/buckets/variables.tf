variable "buckets_config" {

  type = map(object({
    # Ключ - префикс имени бакета ( имя будет заканчиваться на "-bucket" )
    cloud_name = string
    folder_name = string

    sa_list = map(object({
    # Ключ - часть имени сервис аккаунта
      sa_roles      = list(string)
      grant_perms  = list(string)
    }))

    lifecycle_rules = optional(list(object({

      id      = string
      enabled = bool

      abort_incomplete_multipart_upload_days = optional(number)

      transition = optional(object({

        days          = number
        storage_class = string
      }))

      expiration = optional(object({

        days = number
      }))
    })))
    
  }))
}
