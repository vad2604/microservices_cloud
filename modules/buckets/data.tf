data "yandex_resourcemanager_cloud" "this" {
  for_each  = var.buckets_config

  name      = each.value.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  for_each  = var.buckets_config

  name      = each.value.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this[each.key].id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}
