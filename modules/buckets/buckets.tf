resource "yandex_storage_bucket" "this" {
  for_each = var.buckets_config

  bucket = "${each.key}-bucket"

  access_key = yandex_iam_service_account_static_access_key.this[each.key].access_key
  secret_key = yandex_iam_service_account_static_access_key.this[each.key].secret_key

  dynamic "grant" {
    for_each = each.value.sa_list

    content {

      id   = yandex_iam_service_account.this["${bucket_key}.${sa_key}"].id
      type = "CanonicalUser"

      permissions = grant.value.grant_perms
    }
  }

  dynamic "lifecycle_rule" {
    for_each = coalesce(each.value.lifecycle_rules, [])

    content {

      id      = lifecycle_rule.value.id
      enabled = lifecycle_rule.value.enabled

      abort_incomplete_multipart_upload_days = lifecycle_rule.value.abort_incomplete_multipart_upload_days

      dynamic "transition" {
        for_each = coalesce(lifecycle_rule.value.transition, {})

        content {

          days          = lifecycle_rule.value.transition.days
          storage_class = lifecycle_rule.value.transition.storage_class
        }
      }

      dynamic "expiration" {
        for_each = coalesce(lifecycle_rule.value.expiration, {})

        content {
          
          days = lifecycle_rule.value.expiration.days
        }
      }
    }
  }

  depends_on = [
    
    yandex_iam_service_account.this,
    yandex_resourcemanager_folder_iam_member.this,
  ]
}
