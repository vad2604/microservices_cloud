resource "random_password" "user_passwords" {

  for_each          = local.users

  length            = 20
  special           = false
  min_lower         = 3
  min_upper         = 3
}

resource "random_password" "fluent_bit_passwords" {

  for_each = local.kafka_config_full

  length            = 20
  special           = false
  min_lower         = 3
  min_upper         = 3
}

resource "yandex_mdb_kafka_cluster" "this" {

  for_each = local.kafka_config_full

  description = each.value.description

  name        = each.key
  environment = coalesce(each.value.environment, var.defaults.environment)
  network_id  = data.yandex_vpc_network.this[each.key].id
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id

  subnet_ids  = [
    for subnet in each.value.subnet_name : data.yandex_vpc_subnet.this["${each.key}.${subnet}"].id
  ]

  config {

    version          = coalesce(each.value.version, var.defaults.version)
    brokers_count    = coalesce(each.value.brokers_count, var.defaults.brokers_count)
    zones            = each.value.zones
    assign_public_ip = coalesce(each.value.public_access, true)
    unmanaged_topics = coalesce(each.value.unmanaged_topics, false)
    schema_registry  = coalesce(each.value.unmanaged_topics, false)

    kafka {

      resources {

        resource_preset_id = each.value.resource_preset_id
        disk_type_id       = each.value.disk_type_id
        disk_size          = each.value.disk_size
      }

      kafka_config {
        
        compression_type                = each.value.compression_type
        log_flush_interval_messages     = each.value.log_flush_interval_messages
        log_flush_interval_ms           = each.value.log_flush_interval_ms
        log_flush_scheduler_interval_ms = each.value.log_flush_scheduler_interval_ms
        log_retention_bytes             = each.value.log_retention_bytes
        log_retention_hours             = each.value.log_retention_hours
        log_retention_minutes           = each.value.log_retention_minutes
        log_retention_ms                = each.value.log_retention_ms
        log_segment_bytes               = each.value.log_segment_bytes
        log_preallocate                 = each.value.log_preallocate 
        num_partitions                  = each.value.num_partitions
        default_replication_factor      = each.value.default_replication_factor
        message_max_bytes               = each.value.message_max_bytes
        replica_fetch_max_bytes         = each.value.replica_fetch_max_bytes
        ssl_cipher_suites               = each.value.ssl_cipher_suites
        offsets_retention_minutes       = each.value.offsets_retention_minutes
        sasl_enabled_mechanisms         = each.value.sasl_enabled_mechanisms
      }
    }
  }

  labels = each.value.labels
  
  dynamic "user" {

    for_each = each.value.users
    
    content {
      
      name     = user.key
      password = random_password.user_passwords["${each.key}.${user.key}"].result

      dynamic "permission" {
        for_each = user.value.permissions

        content {
          topic_name  = permission.value.topic_name
          role        = permission.value.role
        }
      }

    }
  }

  dynamic "maintenance_window" {
    for_each = {
      for x in ["1"] : x => x
      if each.value.maintenance_window != null
    }

    content {
      type = each.value.maintenance_window.type
      day  = each.value.maintenance_window.day
      hour = each.value.maintenance_window.hour
    }
  }

  security_group_ids = [
   
    yandex_vpc_security_group.this[each.key].id
  ]
  
  deletion_protection = each.value.deletion_protection

  depends_on = [
    yandex_vpc_security_group.this,
    random_password.user_passwords,
    data.yandex_vpc_subnet.access_9091_subnets,
    data.yandex_vpc_subnet.access_9092_subnets,
    data.yandex_vpc_subnet.this
  ]
}

resource "yandex_mdb_kafka_topic" "this" {

  for_each = local.topics

  cluster_id         = yandex_mdb_kafka_cluster.this[each.value.kafka_key].id
  name               = each.value.topic_key
  partitions         = each.value.topic_config.partitions
  replication_factor = each.value.topic_config.replication_factor

  topic_config {

    cleanup_policy        = each.value.topic_config.cleanup_policy
    compression_type      = each.value.topic_config.compression_type
    delete_retention_ms   = each.value.topic_config.delete_retention_ms
    file_delete_delay_ms  = each.value.topic_config.file_delete_delay_ms
    flush_messages        = each.value.topic_config.flush_messages
    flush_ms              = each.value.topic_config.flush_ms
    min_compaction_lag_ms = each.value.topic_config.min_compaction_lag_ms
    retention_bytes       = each.value.topic_config.retention_bytes
    retention_ms          = each.value.topic_config.retention_ms
    max_message_bytes     = each.value.topic_config.max_message_bytes
    min_insync_replicas   = each.value.topic_config.min_insync_replicas
    segment_bytes         = each.value.topic_config.segment_bytes
    preallocate           = each.value.topic_config.preallocate
  }

  depends_on = [
    yandex_mdb_kafka_cluster.this
  ]
}