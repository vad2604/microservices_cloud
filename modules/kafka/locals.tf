locals {

  kafka_config_full = { 
    for kafka_key, kafka_config in var.kafka_config : kafka_key => merge(var.defaults, kafka_config)
  }

  subnets = {

    for entry in distinct(flatten([

      for kafka_key, kafka_config in local.kafka_config_full : [

        for subnet in kafka_config.subnet_name : {

          kafka_key : kafka_key
          subnet    : subnet
        }
      ]
    ])) : "${entry.kafka_key}.${entry.subnet}" => entry
  }

  access_9091_subnets = {

    for entry in distinct(flatten([

      for kafka_key, kafka_config in local.kafka_config_full : [

        for subnet in kafka_config.access_9091_subnets : {

          kafka_key : kafka_key
          subnet    : subnet
        }
      ]
    ])) : "${entry.kafka_key}.${entry.subnet}" => entry
  }

  access_9092_subnets = {

    for entry in distinct(flatten([

      for kafka_key, kafka_config in local.kafka_config_full : [

        for subnet in kafka_config.access_9092_subnets : {

          kafka_key : kafka_key
          subnet    : subnet
        }
      ]
    ])) : "${entry.kafka_key}.${entry.subnet}" => entry
  }

  users = {

    for entry in distinct(flatten([

      for kafka_key, kafka_config in local.kafka_config_full : [

        for user_key, user_config in kafka_config.users : {

          kafka_key   : kafka_key
          user_key    : user_key
          permissions : user_config.permissions
        }
      ]
    ])) : "${entry.kafka_key}.${entry.user_key}" => entry
  }

  topics = {

    for entry in distinct(flatten([

      for kafka_key, kafka_config in local.kafka_config_full : [

        for topic_key, topic_config in kafka_config.topics : {

          kafka_key   : kafka_key
          topic_key   : topic_key
          topic_config: topic_config
        }
      ]
    ])) : "${entry.kafka_key}.${entry.topic_key}" => entry
  }

}