resource "yandex_vpc_security_group" "this" {

  for_each = local.kafka_config_full

  description = "Правила группы обеспечивают базовую работоспособность кластера. Примените ее к кластеру и группам узлов."
  
  name        = "${each.key}-main-sg"
  folder_id   = data.yandex_resourcemanager_folder.this[each.key].id
  network_id  = data.yandex_vpc_network.this[each.key].id

  ingress {
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."

    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  dynamic "ingress" {
    for_each = each.value.access_9091_subnets

    content {
      description    = "Правило разрешает доступ по порту 9091."

      protocol       = "TCP"
      
      v4_cidr_blocks = distinct(flatten([

        data.yandex_vpc_subnet.access_9091_subnets["${each.key}.${ingress.value}"].v4_cidr_blocks
      ]))
      
      port           = 9091
    }
  }

  dynamic "ingress" {
    for_each = each.value.access_9092_subnets

    content {
      description    = "Правило разрешает доступ по порту 9092."

      protocol       = "TCP"
      
      v4_cidr_blocks = distinct(flatten([

        data.yandex_vpc_subnet.access_9092_subnets["${each.key}.${ingress.value}"].v4_cidr_blocks
      ]))
      
      port           = 9092
    }
  }

  dynamic "ingress" {
    for_each = each.value.access_9091_cidrs

    content {
      description    = "Правило разрешает доступ по порту 9091 из сети ${ingress.value}."

      protocol       = "TCP"
      
      v4_cidr_blocks = [ingress.value]
      
      port           = 9091
    }
  }

  dynamic "ingress" {
    for_each = each.value.access_9092_cidrs

    content {
      description    = "Правило разрешает доступ по порту 9092 из сети ${ingress.value}."

      protocol       = "TCP"
      
      v4_cidr_blocks = [ingress.value]
      
      port           = 9092
    }
  }

  egress {
    description    = "Правило разрешает весь исходящий трафик"

    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }

  depends_on = [
    data.yandex_resourcemanager_folder.this,
    data.yandex_vpc_network.this
  ]
}