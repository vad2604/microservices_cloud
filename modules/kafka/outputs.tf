output "user_passwords" {
  value = {

    for kafka_key, kafka_config in local.kafka_config_full: 

    kafka_key => {

      for user in local.users: user.user_key => "${random_password.user_passwords["${user.kafka_key}.${user.user_key}"].result}"
      if user.kafka_key == kafka_key
    }
  }
  sensitive = true
}

output "clusters" {
  value = yandex_mdb_kafka_cluster.this
  sensitive = true
}

output "topics" {
  value = yandex_mdb_kafka_topic.this
}
