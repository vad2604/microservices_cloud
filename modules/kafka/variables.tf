variable "defaults" {
  description = "Значения по-умолчанию для кластера kafkaDB"

  type = object({

    environment = string
    version     = string

    brokers_count = number
  })

  default = {

    environment = "PRODUCTION"
    version     = "3.2"

    brokers_count = 1
  }
}

variable "kafka_config" {
  description = "Переменная, описывающая конфигурацию кластеров kafkaDB"

  type = map(object({
    
    description = string

    cloud_name    = string
    folder_name   = string
    network_name  = string
    environment   = optional(string)
    version       = optional(string)
    deletion_protection = bool
    public_access = bool

    brokers_count     = number
    unmanaged_topics  = bool
    schema_registry   = bool

    subnet_name         = list(string)
    zones               = list(string)
    access_9091_subnets = list(string)
    access_9092_subnets = list(string)
    access_9091_cidrs   = optional(list(string))
    access_9092_cidrs   = optional(list(string))

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    compression_type                = optional(string)
    log_flush_interval_messages     = optional(number)
    log_flush_interval_ms           = optional(number)
    log_flush_scheduler_interval_ms = optional(number)
    log_retention_bytes             = optional(number)
    log_retention_hours             = optional(number)
    log_retention_minutes           = optional(number)
    log_retention_ms                = optional(number)
    log_segment_bytes               = optional(number)
    log_preallocate                 = optional(bool)
    num_partitions                  = optional(number)
    default_replication_factor      = optional(number)
    message_max_bytes               = optional(number)
    replica_fetch_max_bytes         = optional(number)
    ssl_cipher_suites               = optional(list(string))
    offsets_retention_minutes       = optional(number)
    sasl_enabled_mechanisms         = optional(list(string)) 

    maintenance_window = optional(object({

      type = string
      day  = string
      hour = number
    }))

    users = map(object({

      permissions = set(object({

        topic_name = string
        role       = string
      }))
    }))

    topics = map(object({

      partitions            = number
      replication_factor    = number

      cleanup_policy        = optional(string)
      compression_type      = optional(string)
      delete_retention_ms   = optional(number)
      file_delete_delay_ms  = optional(number)
      flush_messages        = optional(number)
      flush_ms              = optional(number)
      min_compaction_lag_ms = optional(number)
      retention_bytes       = optional(number)
      retention_ms          = optional(number)
      max_message_bytes     = optional(number)
      min_insync_replicas   = optional(number)
      segment_bytes         = optional(number)
      preallocate           = optional(bool)
    }))

    labels      = map(string)

  }))

}
