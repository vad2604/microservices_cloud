variable "defaults" {
  description = "Значения по-умолчанию для раннеров"

  type = object({
    
    create_sa: bool
    sa_name: string
    concurrent: number
    check_interval: number
    priviledged: bool
    prometheus_metrics: bool
    default_image: string
    runner_locked: bool
    run_untagged_jobs: bool
    version: string
    namespace: string
    create_ns: bool
    helm_repo: string
  })

  default = {

    helm_repo = "https://charts.gitlab.io"
    check_interval = 5
    concurrent = 10
    create_sa = true
    sa_name = "gitlab-runner"
    default_image = "alpine:3.17"
    priviledged = true
    prometheus_metrics = false
    runner_locked = true
    run_untagged_jobs = false
    
    version = "0.49.0"
    namespace = "gitlab-runner"
    create_ns = true
  }
  
}

variable "gitlab_runner_config" {
  description = "Конфигурация раннеров"

  type = object({

    name: optional(string)
    version: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    helm_repo: optional(string)

    runners: map(object({
      # ключ - имя раннера
      url: string
      token: string
      tags: list(string)
      create_sa: optional(bool)
      sa_name: optional(string)
      concurrent: optional(number)
      check_interval: optional(number)
      cpu_request: optional(string)
      memory_request: optional(string)
      priviledged: optional(bool)
      prometheus_metrics: optional(bool)
      default_image: optional(string)
      runner_locked: optional(bool)
      run_untagged_jobs: optional(bool)
      node_pool: optional(map(string))
    }))
    
    pull_secrets: map(object({
      # ключ - имя секрета
      server: string
      username: string
      password: string
      namespaces: list(string)
    }))

  })
}
