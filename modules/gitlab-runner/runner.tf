resource "helm_release" "this" {
  
  for_each = var.gitlab_runner_config.runners
  
  name             = "${each.key}"
  repository       = coalesce(var.gitlab_runner_config.helm_repo, var.defaults.helm_repo)
  chart            = "gitlab-runner"
  create_namespace = coalesce(var.gitlab_runner_config.create_ns, var.defaults.create_ns)
  namespace        = coalesce(var.gitlab_runner_config.namespace, var.defaults.namespace)
  version          = coalesce(var.gitlab_runner_config.version, var.defaults.version)
  max_history      = 3
  atomic           = true
  recreate_pods    = false
  force_update     = false

  values = [
    <<EOF

gitlabUrl: "${each.value.url}"
runnerRegistrationToken: "${each.value.token}"
concurrent: "${coalesce(each.value.concurrent, var.defaults.concurrent)}"
checkInterval: ${coalesce(each.value.check_interval, var.defaults.check_interval)}
rbac:
  create: ${coalesce(each.value.create_sa, var.defaults.create_sa)}
  serviceAccountName: "${each.key}-${coalesce(each.value.sa_name, var.defaults.sa_name)}"
  rules:
    - apiGroups: [""]
      resources: ["pods"]
      verbs: ["list", "get", "watch", "create", "delete"]
    - apiGroups: [""]
      resources: ["pods/exec"]
      verbs: ["create"]
    - apiGroups: [""]
      resources: ["pods/log"]
      verbs: ["get"]
    - apiGroups: [""]
      resources: ["secrets"]
      verbs: ["list", "get", "create", "delete", "update"]
    - apiGroups: [""]
      resources: ["configmaps"]
      verbs: ["list", "get", "create", "delete", "update"]
    - apiGroups: [""]
      resources: ["pods/attach"]
      verbs: ["list", "get", "create", "delete", "update"]
    - apiGroups: [""]
      resources: ["serviceaccounts"]
      verbs: ["list", "get", "create", "delete", "update"]

metrics:
  serviceMonitor:
    enabled: ${coalesce(each.value.prometheus_metrics, var.defaults.prometheus_metrics)}
  
runners:
  namespace: "${coalesce(var.gitlab_runner_config.namespace, var.defaults.namespace)}"
  privileged: ${coalesce(each.value.priviledged, var.defaults.priviledged)}   
  name: "${each.key}"
  tags: "${join(",", each.value.tags)}"
  runUntagged: "${coalesce(each.value.run_untagged_jobs, var.defaults.run_untagged_jobs)}"
  locked: "${coalesce(each.value.runner_locked, var.defaults.runner_locked)}"
  serviceAccountName: "${each.key}-${coalesce(each.value.sa_name, var.defaults.sa_name)}"
  config: |
    concurrent = ${coalesce(each.value.concurrent, var.defaults.concurrent)}
    check_interval = ${coalesce(each.value.check_interval, var.defaults.check_interval)}
    %{ if each.value.cpu_request != null ~}
    cpu_request = "${each.value.cpu_request}"
    %{ endif ~} 
    %{ if each.value.memory_request != null ~}
    memory_request = "${each.value.memory_request}"
    %{ endif ~} 
    [[runners]]    
      limit = ${coalesce(each.value.concurrent, var.defaults.concurrent)}
      request_concurrency = ${coalesce(each.value.concurrent, var.defaults.concurrent)}
      [runners.kubernetes]
        image = "${coalesce(each.value.default_image, var.defaults.default_image)}"
        service_account = "${each.key}-${coalesce(each.value.sa_name, var.defaults.sa_name)}"
        namespace = "${coalesce(var.gitlab_runner_config.namespace, var.defaults.namespace)}"
        image_pull_secrets = ${jsonencode(local.pull_secrets)}
        priviledged = ${coalesce(each.value.priviledged, var.defaults.priviledged)}    
        %{ if each.value.node_pool != null ~}
        [runners.kubernetes.node_selector]
        %{ for selector, value in each.value.node_pool ~}
          "${selector}" = "${value}"
        %{ endfor ~}
        %{ endif ~} 
    
    EOF
    ,
  ]

}

resource "kubernetes_secret" "this" {
  
  for_each = local.pull_secret_namespaces
  
  metadata {
    name = each.value.name
    namespace = each.value.namespace
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${each.value.server}" = {
          auth = "${base64encode("${each.value.username}:${each.value.password}")}"
          password = "${each.value.password}"
          username = "${each.value.username}"
        }
      }
    })
  }

  depends_on = [
    helm_release.this
  ]
}
