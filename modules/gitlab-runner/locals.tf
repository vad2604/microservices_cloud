locals {

  pull_secret_namespaces = {

    for entry in distinct(flatten([

      for ps_key, ps_config in var.gitlab_runner_config.pull_secrets : [

        for ps_ns in ps_config.namespaces : {

          name      : ps_key
          namespace : ps_ns
          server    : ps_config.server
          username  : ps_config.username
          password  : ps_config.password
        }
      ]
    ])) : "${entry.name}.${entry.namespace}" => entry
  }

  pull_secrets = [
    for ps_key, ps_config in var.gitlab_runner_config.pull_secrets : ps_key
  ]

}