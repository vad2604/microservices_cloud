data "yandex_resourcemanager_cloud" "this" {

  count = var.ingress_config.dns_zone_name == null || var.ingress_config.folder_name == null || var.ingress_config.cloud_name == null ? 0 : 1

  name  = var.ingress_config.cloud_name
}

data "yandex_resourcemanager_folder" "this" {

  count       = var.ingress_config.dns_zone_name == null || var.ingress_config.folder_name == null || var.ingress_config.cloud_name == null ? 0 : 1

  name        = var.ingress_config.folder_name
  cloud_id    = data.yandex_resourcemanager_cloud.this[0].id

  depends_on  = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "yandex_dns_zone" "this" {

  count       = var.ingress_config.dns_zone_name == null || var.ingress_config.folder_name == null || var.ingress_config.cloud_name == null ? 0 : 1

  name        = var.ingress_config.dns_zone_name
  folder_id   = data.yandex_resourcemanager_folder.this[0].id

  depends_on  = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "kubernetes_service" "this" {
  metadata {
    name      = var.ingress_config.name == null ? "ingress-nginx-controller" : "${var.ingress_config.name}-ingress-nginx-controller"
    namespace = coalesce(var.ingress_config.namespace, "ingress-nginx")
  }

  depends_on = [
    helm_release.this,
    time_sleep.this
  ]
}
