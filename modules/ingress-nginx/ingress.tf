resource "helm_release" "this" {

  name              = coalesce(var.ingress_config.name, "ingress-nginx")
  repository        = "https://kubernetes.github.io/ingress-nginx"
  chart             = "ingress-nginx"
  version           = coalesce(var.ingress_config.version, "4.4.2")
  namespace         = coalesce(var.ingress_config.namespace, "ingress-nginx")

  max_history       = 3
  atomic            = true
  create_namespace  = coalesce(var.ingress_config.create_ns, true)
  recreate_pods     = true
  force_update      = true
  timeout           = "1200"

  values = [
    <<EOF

controller:

  replicaCount: "${var.ingress_config.replicas}"
  minAvailable: "${var.ingress_config.replicas}"

  config:
    client-max-body-size: "1024m"
    proxy-body-size: "1024m"
    proxy-read-timeout: "120"
    proxy-connect-timeout: "10"
    proxy-send-timeout: "120"
    use-forwarded-headers: "true"
    use-proxy-protocol: "false"
    proxy-real-ip-cidr: "0.0.0.0/0"
    proxy-buffer-size: "128k"
    ignore-invalid-headers: "false"
    client-header-buffer-size: "128k"
    http2-max-header-size: "128k"
    proxy-headers-hash-max-size: "2048"
    proxy-headers-hash-bucket-size: "512"
    proxy-protocol-header-timeout: "20s"

  service:
    enabled: true
    appProtocol: true
    external:
      enabled: true
    externalTrafficPolicy: Cluster
    annotations:
      cert-manager.io/cluster-issuer: "${coalesce(var.ingress_config.acme_issuer,"letsencrypt")}"
      acme.cert-manager.io/http01-edit-in-place: "true"
      ingressclass.kubernetes.io/is-default-class: "true"
    internal:
      enabled: false
      externalTrafficPolicy: Cluster
      annotations: {}
    labels: {}
    enableHttp: true
    enableHttps: true
    ipFamilyPolicy: "SingleStack"
    ipFamilies:
      - IPv4
    ports:
      http: 80
      https: 443
    nodePorts:
      http: 32080
      https: 32443
    targetPorts:
      http: http
      https: https
    type: LoadBalancer

  admissionWebhooks:
    enabled: false

  metrics:
    port: 10254
    enabled: true

    service:
      type: ClusterIP
      
    serviceMonitor:
      enabled: ${coalesce(var.ingress_config.prometheus_metrics, "false")}
      namespace: "${coalesce(var.ingress_config.namespace, "ingress-nginx")}"

revisionHistoryLimit: 10

defaultBackend:
  enabled: false

rbac:
  create: true
  scope: false

podSecurityPolicy:
  enabled: false

serviceAccount:
  create: true
  name: "${coalesce(var.ingress_config.name, "ingress-nginx")}"
  automountServiceAccountToken: true

    EOF
    ,
  ]
}

resource "time_sleep" "this" {
  # после создания игресса выжидаем 5 секунд, перед получением данных о сервисе 
  create_duration = "5s"

  depends_on = [
    helm_release.this
  ]
}

resource "yandex_dns_recordset" "this" {

  count       = var.ingress_config.dns_zone_name == null || var.ingress_config.folder_name == null || var.ingress_config.cloud_name == null ? 0 : 1

  zone_id = data.yandex_dns_zone.this[0].id
  name    = "*"
  type    = "A"
  ttl     = 600
  data    = [data.kubernetes_service.this.status[0].load_balancer[0].ingress[0].ip]
}
