variable "ingress_config" {
  type = object({
    
    replicas: string
    
    cloud_name: optional(string)
    folder_name: optional(string)
    dns_zone_name: optional(string)
    acme_issuer: optional(string)
    name: optional(string)
    version: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    prometheus_metrics: optional(bool)
  })
}
