resource "yandex_iam_service_account" "this" {
  description = "Сервис аккаунт для Fluent-Bit"

  name        = coalesce(var.fluent_bit_config.sa_name, "k8s-fluent-bit-sa")
  folder_id   = data.yandex_resourcemanager_folder.this.id
}

resource "yandex_resourcemanager_folder_iam_member" "this" {
  for_each = toset(local.sa_roles)

  role       = each.value
  member     = "serviceAccount:${yandex_iam_service_account.this.id}"
  folder_id  = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    yandex_iam_service_account.this,
  ]
}

resource "yandex_iam_service_account_key" "this" {
  description        = "Ключ сервис-аккаунта для Fluent-Bit"

  service_account_id = yandex_iam_service_account.this.id
  key_algorithm      = "RSA_2048"

  depends_on = [
    yandex_iam_service_account.this,
    yandex_resourcemanager_folder_iam_member.this,
  ]
}

