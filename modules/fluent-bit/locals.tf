locals {
  ############
  # Defaults
  ############
  default_sa_roles = [
    "logging.writer",
    "monitoring.editor",
    "container-registry.images.puller"
  ]

  sa_roles = coalesce(var.fluent_bit_config.sa_roles, local.default_sa_roles)

  kafka_endpoints = [

    for host in data.yandex_mdb_kafka_cluster.this[0].host : "${host.name}:9091"
    if var.fluent_bit_config.kafka_cluster_name != null
  ]
}
