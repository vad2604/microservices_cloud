variable "kafka_password" {
  type = string
}

variable "yandex_cloud_ca_url" {
  type = string
  default = "https://storage.yandexcloud.net/cloud-certs/CA.pem"
}

variable "fluent_bit_config" {
  description = "Конфигурация Fluent-Bit"
  
  type = object({
    
    cloud_name: string
    folder_name: string
    yc_logging_name: optional(string)
    kafka_cluster_name: optional(string)
    kafka_topic_name: optional(string)
    kafka_user: optional(string)

    name: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    sa_name: optional(string)
    sa_roles: optional(list(string))
  })
}