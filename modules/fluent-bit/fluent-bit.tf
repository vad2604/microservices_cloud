resource "helm_release" "this" {

  name              = coalesce(var.fluent_bit_config.name, "fluent-bit")

  chart             = "${path.module}/files/fluent-bit-cloud-logging-1.0-6-with-kafka-support.tgz"
  namespace         = coalesce(var.fluent_bit_config.namespace, "fluent-bit")

  max_history       = 3
  atomic            = true
  create_namespace  = coalesce(var.fluent_bit_config.create_ns, true)
  recreate_pods     = true
  force_update      = true

  values = [
    <<EOF

image: cr.yandex/yc-marketplace/yandex-cloud/fluent-bit/cloud-logging:2.0.2-1.9.3

kafka: 
  enabled: ${var.fluent_bit_config.kafka_cluster_name == null ? "false" : "true"}
  endpoints: "${var.fluent_bit_config.kafka_cluster_name == null ? "" : join(",", local.kafka_endpoints) }"
  topic: "${var.fluent_bit_config.kafka_cluster_name == null ? "" : var.fluent_bit_config.kafka_topic_name}"
  user: "${var.fluent_bit_config.kafka_cluster_name == null ? "" : var.fluent_bit_config.kafka_user}"
  password: "${var.fluent_bit_config.kafka_cluster_name == null ? "" : var.kafka_password}"
  ca_crt: |
    ${var.fluent_bit_config.kafka_cluster_name == null ? "nil" : indent(4, data.http.yc_cl_ca.response_body)}

ycLogging:
  enabled: ${var.fluent_bit_config.yc_logging_name == null ? "false" : "true"}
  id: "${var.fluent_bit_config.yc_logging_name == null ? "" : data.yandex_logging_group.this[0].id}"
  auth:
    json: |
      ${var.fluent_bit_config.yc_logging_name == null ? "{}" : jsonencode({
        id                  = "${yandex_iam_service_account_key.this.id}"
        service_account_id  = "${yandex_iam_service_account_key.this.service_account_id}"
        created_at          = "${yandex_iam_service_account_key.this.created_at}"
        key_algorithm       = "${yandex_iam_service_account_key.this.key_algorithm}"
        public_key          = "${yandex_iam_service_account_key.this.public_key}"
        private_key         = "${yandex_iam_service_account_key.this.private_key}"
      })}

objectStorage:
  enabled: false
  bucket: ""
  serviceaccountawskeyvalue: string
  serviceaccountawskeyvalue_generated:
    accessKeyID: string
    secretAccessKey: string

input:
  tag: "kube.*"
  path: "/var/log/containers/*.log"
  db: "/var/log/flb_kube.db"
  parser: cri-log
  dockerMode: "Off"
  memBufLimit: 50MB
  skipLongLines: "Off"
  refreshInterval: 2

filter:
  match: "kube.*"
  mergeLog: "On"
  mergeLogKey: "data"
  keepLog: "On"
  k8sLoggingParser: "On"
  k8sLoggingExclude: "On"
  bufferSize: "32k"


EOF

  ]

  depends_on = [
    yandex_iam_service_account_key.this,
    data.yandex_resourcemanager_folder.this,
    data.yandex_mdb_kafka_cluster.this,
    data.yandex_mdb_kafka_topic.this,
    data.yandex_logging_group.this,
    data.http.yc_cl_ca,
  ]
}