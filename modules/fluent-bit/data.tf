data "yandex_resourcemanager_cloud" "this" {

  name      = var.fluent_bit_config.cloud_name
}

data "yandex_resourcemanager_folder" "this" {

  name      = var.fluent_bit_config.folder_name
  cloud_id  = data.yandex_resourcemanager_cloud.this.id

  depends_on = [
    data.yandex_resourcemanager_cloud.this
  ]
}

data "yandex_mdb_kafka_cluster" "this" {

  count = var.fluent_bit_config.kafka_cluster_name == null ? 0 : 1

  name        = var.fluent_bit_config.kafka_cluster_name
  folder_id   = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "yandex_mdb_kafka_topic" "this" {

  count = var.fluent_bit_config.kafka_cluster_name == null ? 0 : 1

  name        = var.fluent_bit_config.kafka_topic_name
  cluster_id  = data.yandex_mdb_kafka_cluster.this[0].id

  depends_on = [
    data.yandex_mdb_kafka_cluster.this
  ]
}

data "yandex_logging_group" "this" {

  count = var.fluent_bit_config.yc_logging_name == null ? 0 : 1

  name        = var.fluent_bit_config.kafka_cluster_name
  folder_id   = data.yandex_resourcemanager_folder.this.id

  depends_on = [
    data.yandex_resourcemanager_folder.this
  ]
}

data "http" "yc_cl_ca" {
  # получение биллинг аккаунта
  url = var.yandex_cloud_ca_url
  method = "GET"
}