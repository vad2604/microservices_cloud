output "clouds" {
    description = "Вывод информации о облаках"

    value = jsondecode(jsonencode(module.clouds))
}

output "folders" {
    description = "Вывод информации о фолдерах"

    value = jsondecode(jsonencode(module.folders))
}

output "dns_zones" {
    description = "Вывод информации о сетях"

    value = jsondecode(jsonencode(module.dns_zones))
}

output "networks" {
    description = "Вывод информации о сетях"

    value = jsondecode(jsonencode(module.networks))
}

output "buckets" {
    description = "Вывод информации об объектных хранилищах"

    value = jsondecode(jsonencode(module.buckets))
    sensitive = true
}

output "logging_groups" {
    description = "Вывод информации о Лог-группах"

    value = jsondecode(jsonencode(module.logging_groups))
}

output "psql" {
    description = "Вывод информации о PSQL"
    
    value = jsondecode(jsonencode(module.psql))
    sensitive = true
}

output "mongo" {
    description = "Вывод информации о MongoDB"
    
    value = jsondecode(jsonencode(module.mongodb))
    sensitive = true
}

output "kafka" {
    description = "Вывод информации о MongoDB"
    
    value = jsondecode(jsonencode(module.kafka))
    sensitive = true
}

output "k8s" {
    description = "Вывод информации о K8s"

    value = jsondecode(jsonencode(module.k8s))
    sensitive = true
}
