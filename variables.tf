variable "organization_id" {
  description = "ID организации"

  type = string
}

variable "token" {
  description = "OAuth-токен владельца организации"
  
  type = string
  sensitive   = true
}

variable "yc_iam_tokens_api" {
  description = "API YCloud по управлению IAM-токенами"

  type    = string
  default = "https://iam.api.cloud.yandex.net/iam/v1/tokens"
}

variable "clouds_config" {
  description = "Описание облаков, которые создаются/управляются из terraform"

  # определение типа для данной переменной
  type = map(object({
    # ключ = имя облака
    description: string

    labels: map(string)

    cloud_users: map(object({
      cloud_user_roles: list(string)
    }))

  }))
}

variable "folders_config" {
  description = "Описание фолдеров, которые создаются/управляются из terraform"
  
  # определение типа для данной переменной
  type = map(object({
    # ключ = имя фолдера
    description : string
    
    cloud_name: string
    labels: map(string)

    folder_users: map(object({
      folder_user_roles: list(string)
    }))
    
  }))
}

variable "cloud_logging_config" {
  description = "Конфигуация для сервиса logging в Yandex Cloud"

  type = map(object({
    # ключ = имя группы
    cloud_name       = string
    folder_name      = string
    description      = optional(string)
    labels           = optional(map(string))
    retention_period = optional(string)
  }))

}

variable "dns_zones_config" {
  # определение типа для данной переменной
  type = map(object({
    # ключ = имя зоны
    description: string
    dns_zone: string
    public: bool
    cloud_name: string
    folder_name: string

    labels: map(string)
  
  }))

}

variable "networks_config" {
  description = "Конфигурация сети инфраструктуры"
  
  type = map(object({    
    # Ключ - имя сети
    description: string
    
    cloud_name: string
    folder_name: string
    gateway_description: string
    route_table_description: string

    subnets: map(object({
      # Ключ - имя подсети
      v4_cidr_blocks: list(string)
      availability_zone: string
      use_nat: bool
    }))

    labels: map(string)

  }))

}

variable "k8s_config" {
  description = "Переменная для конфигурирования кластеров и их node groups"

  type = object({
    description: string
    name: string

    cloud_name   : string
    folder_name  : string
    network_name : string

    ns_create_list: list(string) # список неймспейсов для создания

    zonal_regional: string # в зональной конфигурации возьмётся зона первой подсети
    cluster_sa_roles: list(string)
    node_groups_sa_roles: list(string)
    k8s_master_version: string
    maintenance_window_day: string
    maintenance_window_start_time: string
    maintenance_window_duration: string
    prevent_destroy: bool
    
    public_ip: bool
    cluster_ipv4_range: string
    service_ipv4_range: string
    subnet_names: list(string) # в зональной конфигурации возьмётся зона первой подсети
    access_22_cidrs: list(string)
    access_API_cidrs: list(string)
    access_22_subnets: list(string)
    access_API_subnets: list(string)

    access_ingress_whitelist: map(object({
      description: string

      v4_cidr_blocks: list(string)
      from_port: number
      to_port: number
      protocol: string
    }))

    labels: map(string)

    region: optional(string)
    master_auto_upgrade: optional(bool)
    release_channel: optional(string)
    network_policy_provider: optional(string)

    node_groups: map(object({
      # Ключ - имя node group
      description: string

      #subnet_name: list(string) # в зональной конфигурации возьмётся зона первой подсети
      k8s_node_version: string
      resources_memory: number
      resources_cpu_cores: number
      boot_disk_type: string
      boot_disk_size: number
      current_node_count: number
      auto_scale_min_count: optional(number)
      auto_scale_max_count: optional(number)
      maintenance_window_day: string
      maintenance_window_start_time: string
      maintenance_window_duration: string
      node_taints: optional(list(string))
      network_acceleration: optional(bool)

      labels: map(string)

      instance_template_platform_id: optional(string)
      maintenance_policy_auto_upgrade: optional(bool)
      maintenance_policy_auto_repair: optional(bool)
      preemptible: optional(bool)
      node_labels: optional(map(string))
    }))
    
  })
}

variable "buckets_config" {

  type = map(object({
    # Ключ - префикс имени бакета ( имя будет заканчиваться на "-bucket" )
    cloud_name = string
    folder_name = string

    sa_list = map(object({
    # Ключ - часть имени сервис аккаунта
      sa_roles      = list(string)
      grant_perms  = list(string)
    }))

    lifecycle_rules = optional(list(object({

      id      = string
      enabled = bool

      abort_incomplete_multipart_upload_days = optional(number)

      transition = optional(object({

        days          = number
        storage_class = string
      }))

      expiration = optional(object({

        days = number
      }))
    })))
  }))
}

variable "psql_config" {
  description = "Переменная, описывающая конфигурацию кластеров постгреса"

  type = map(object({
    
    description = string

    cloud_name          = string
    folder_name         = string
    network_name        = string
    admin_name          = string
    public_access       = optional(bool)

    hosts               = map(object({
      
      subnet_name             = string
      host_name               = optional(string)
      priority                = optional(number)
      replication_source_name = optional(string)
      replication_source      = optional(string)
    }))

    access_6432_subnets = list(string)
    access_6432_cidrs   = optional(list(string))

    environment = optional(string)
    version     = optional(string)

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number
    deletion_protection = bool
    user_conn_limit     = optional(number)

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = optional(object({

      type = string
      day  = optional(string)
      hour = optional(number)
    }))
    
    users = map(object({

      conn_limit           = optional(number)
      accessible_databases = list(string)
      grants               = optional(list(string))
    }))

    pool_discard = optional(bool)
    pooling_mode = optional(string)
     
    max_connections                   = optional(number)
    enable_parallel_hash              = optional(bool)
    vacuum_cleanup_index_scale_factor = optional(number)
    autovacuum_vacuum_scale_factor    = optional(number)
    default_transaction_isolation     = optional(string)
    shared_preload_libraries          = optional(string)

    labels = map(string)

    databases = map(object({
      # Конфигурация баз данных кластера psql
      lc_collate    = optional(string)
      lc_type       = optional(string)
      owner         = string
      extension     = list(string)
    }))

  }))
}

variable "mongo_config" {
  description = "Переменная, описывающая конфигурацию кластеров MongoDB"

  type = map(object({
    
    description = string

    cloud_name    = string
    folder_name   = string
    network_name  = string
    environment   = optional(string)
    version       = optional(string)
    deletion_protection = bool
    public_access = bool

    subnet_name          = list(string)
    access_27018_subnets = list(string)
    access_27018_cidrs   = optional(list(string))

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    backup_window_start = object({

      hours   = number
      minutes = number
    })

    maintenance_window = optional(object({

      type = string
      day  = string
      hour = number
    }))
    
    databases = list(string)

    users = map(object({

      accessible_databases = set(object({

        database_name = string
        # https://cloud.yandex.com/en-ru/docs/managed-mongodb/concepts/users-and-roles
        roles         = list(string)
      }))
    }))

    labels      = map(string)
    
  }))

}

variable "kafka_config" {
  description = "Переменная, описывающая конфигурацию кластеров kafkaDB"

  type = map(object({
    
    description = string

    cloud_name    = string
    folder_name   = string
    network_name  = string
    environment   = optional(string)
    version       = optional(string)
    deletion_protection = bool
    public_access = bool

    brokers_count     = number
    unmanaged_topics  = bool
    schema_registry   = bool

    subnet_name         = list(string)
    zones               = list(string)
    access_9091_subnets = list(string)
    access_9092_subnets = list(string)
    access_9091_cidrs   = optional(list(string))
    access_9092_cidrs   = optional(list(string))

    resource_preset_id = string
    disk_type_id       = string
    disk_size          = number

    compression_type                = optional(string)
    log_flush_interval_messages     = optional(number)
    log_flush_interval_ms           = optional(number)
    log_flush_scheduler_interval_ms = optional(number)
    log_retention_bytes             = optional(number)
    log_retention_hours             = optional(number)
    log_retention_minutes           = optional(number)
    log_retention_ms                = optional(number)
    log_segment_bytes               = optional(number)
    log_preallocate                 = optional(bool)
    num_partitions                  = optional(number)
    default_replication_factor      = optional(number)
    message_max_bytes               = optional(number)
    replica_fetch_max_bytes         = optional(number)
    ssl_cipher_suites               = optional(list(string))
    offsets_retention_minutes       = optional(number)
    sasl_enabled_mechanisms         = optional(list(string)) 

    maintenance_window = optional(object({

      type = string
      day  = string
      hour = number
    }))

    users = map(object({

      permissions = set(object({

        topic_name = string
        role       = string
      }))
    }))

    topics = map(object({

      partitions            = number
      replication_factor    = number

      cleanup_policy        = optional(string)
      compression_type      = optional(string)
      delete_retention_ms   = optional(number)
      file_delete_delay_ms  = optional(number)
      flush_messages        = optional(number)
      flush_ms              = optional(number)
      min_compaction_lag_ms = optional(number)
      retention_bytes       = optional(number)
      retention_ms          = optional(number)
      max_message_bytes     = optional(number)
      min_insync_replicas   = optional(number)
      segment_bytes         = optional(number)
      preallocate           = optional(bool)
    }))

    labels      = map(string)

  }))
  
}

variable "cert_manager_config" {

  type = object({

    email                   : string
    issuer                  : optional(string)
    private_key_secret_name : optional(string)
    provider_url            : optional(string)
  })
}

variable "fluent_bit_config" {
  description = "Конфигурация Fluent-Bit"
  
  type = object({
    
    cloud_name: string
    folder_name: string
    yc_logging_name: optional(string)
    kafka_cluster_name: optional(string)
    kafka_topic_name: optional(string)
    kafka_user: optional(string)

    name: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    sa_name: optional(string)
    sa_roles: optional(list(string))
  })
}

variable "ingress_config" {
  type = object({
    
    replicas: string
    
    cloud_name: optional(string)
    folder_name: optional(string)
    dns_zone_name: optional(string)
    acme_issuer: optional(string)
    name: optional(string)
    version: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    prometheus_metrics: optional(bool)
  })
}

variable "gitlab_runner_config" {
  description = "Конфигурация раннеров"

  type = object({

    name: optional(string)
    version: optional(string)
    namespace: optional(string)
    create_ns: optional(bool)
    helm_repo: optional(string)

    runners: map(object({
      # ключ - имя раннера
      url: string
      token: optional(string)
      tags: list(string)
      create_sa: optional(bool)
      sa_name: optional(string)
      concurrent: optional(number)
      check_interval: optional(number)
      cpu_request: optional(string)
      memory_request: optional(string)
      priviledged: optional(bool)
      prometheus_metrics: optional(bool)
      default_image: optional(string)
      runner_locked: optional(bool)
      run_untagged_jobs: optional(bool)
      node_pool: optional(map(string))
    }))
    
    pull_secrets: map(object({
      # ключ - имя секрета
      server: string
      username: string
      password: optional(string)
      namespaces: list(string)
    }))

  })
}

variable "runner_tokens" {
  type = map(string)
  default = {}
}

variable "pull_secret_passwords" {
  type = map(string)
  default = {}
}